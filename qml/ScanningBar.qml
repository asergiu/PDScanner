import QtQuick 2.0

Item{

    id: root
    z: 1

    property alias rectangleGradient: progressRect.gradient
    property alias rectangleOpacity: progressRect.opacity
    property alias barHeight: progressBar.height
    property alias barColor: progressBar.color
    property bool runToEnd: false

    signal animationEnded();

    onAnimationEnded: root.visible = false;

    onVisibleChanged: {
        if(visible == false)
            progressAnimation.stop();
    }

    onHeightChanged: startAnimation();

    function startAnimation(){
        privateMembers.stopAnimation = false;
        privateMembers.loopEnded = false;
        progressAnimation.from = 0;
        progressAnimation.to = root.height;
        progressAnimation.restart();
    }

    function stopAnimation(){
        privateMembers.stopAnimation = true;
        if(runToEnd && !privateMembers.loopEnded) return;
        progressAnimation.stop();
    }

    QtObject{
        id: privateMembers
        property bool stopAnimation: false
        property bool loopEnded: false
    }

    Rectangle{
        id: progressRect


        width: parent.width
        height: 1
        gradient: Gradient{
            GradientStop{
                position: 0
                color: "white"
            }
            GradientStop{
                position: 1
                color: "black"
            }
        }
        opacity: 0.5
    }

    Rectangle{
        id: progressBar

        anchors{
            left: progressRect.left
            right: progressRect.right
            top: progressRect.bottom
        }
        height: 4

        color: "black"
    }


    NumberAnimation {
        id: progressAnimation

        target: progressRect;
        property: "height";
        from: 0;
        to: root.height;
        duration: 2000;
        easing.type: Easing.InOutQuad
        alwaysRunToEnd: true

        onStopped: {
            if(privateMembers.stopAnimation){
                animationEnded();
                return;
            }
            // switch animation direction
            if (progressRect.height == root.height) {
                progressAnimation.from = root.height;
                progressAnimation.to = 0;
                privateMembers.loopEnded = true;
            } else if (progressRect.height == 0) {
                progressAnimation.from = 0;
                progressAnimation.to = root.height;
            }
            progressAnimation.start();
        }

    }

}
