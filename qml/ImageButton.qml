import QtQuick 2.0
import "../js/Constants.js" as Constants

Image {
    id: root

    property string image: Constants.BlueButton
    property string pressedImage: Constants.BlueButtonTap
    property alias text: buttonText.text

    source: image

    fillMode: Image.PreserveAspectFit
    width: 300

    signal clicked();

    onEnabledChanged: {
        if (enabled)
            root.source = image;
        else
            root.source = pressedImage;
    }

    Text {
        id: buttonText
        font.pointSize: 15
        anchors.centerIn: parent
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        onClicked: {
            root.clicked()
        }

        onPressed: {
            root.source = pressedImage;
        }

        onReleased: {
            root.source = root.image;
        }
    }
}
