import QtQuick 2.0
import "../js/Constants.js" as Constants

Image {
    id: root
    width: 1024
    height: 768

    source: Constants.BackgroundImage

    QtObject {
        id: internalVars

        property bool measurementEnabled: true
        property string initialMessage: qsTr("<center>Stand centered in front of the mirror, and press START!</center><br>Follow the green light with your eyes only, without moving!")
        property string detectionFailedMessage: qsTr("<center>Eyes not detected!</center><br>Please adjust your position in front of the PDScanner and try again!");
        property string measurementInProcessMessage: qsTr("Measurement in process. Please follow the green light!");
    }

    Image {
        id: roundLogo
        width: parent.width / 3.3
        height: width
        anchors { top: parent.top; left: parent.left; margins: 70; }
        source: Constants.RoundLogo
    }

    Image {
        id: acepLogo
        fillMode: Image.PreserveAspectFit
        width: parent.width / 6
        anchors { bottom: parent.bottom; left: parent.left; margins: 30; }
        source: Constants.AcepLogo
    }

    Column {
        id: buttonsColumn

        anchors { right: parent.right; top: parent.top; topMargin: 60; rightMargin: 30; }

        ImageButton {
            id: startMeasurementButton
            text: qsTr("START")
            enabled: internalVars.measurementEnabled

            onClicked: {
                if(videoController.startNewMeasurement()) {
                    internalVars.measurementEnabled = false;
                    notificationMessage.text = internalVars.measurementInProcessMessage;
                    notificationMessage.visible = true;
                    messageTimer.start();
                    infoGrid.visible = false;
                    showInitialMessageTimer.stop();
                }
            }
        }

        ImageButton {
            id: resetButton
            text: qsTr("Reset Position")

            onClicked: {
                videoController.stopMeasurement();
                notificationMessage.text = internalVars.initialMessage;
                notificationMessage.visible = true;
                internalVars.measurementEnabled = true;
                showInitialMessageTimer.stop();
            }
        }
    }

    Grid {
        id: infoGrid
        spacing: 20
        anchors { horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: 100; }
        columns: 2
        visible: false

        property int textSize: 20

        Text {
            id: pdLabel
            text: qsTr("Your PD:")
            font.pointSize: parent.textSize + 2
        }

        Text {
            id: pdValue
            text: "0"
            font.pointSize: parent.textSize + 2
        }

        Text {
            id: leftDist
            textFormat: Text.RichText
            text: qsTr("Z<sub>L</sub>:")
            font.pointSize: parent.textSize
        }

        Text {
            id: leftDistValue
            text: "0"
            font.pointSize: parent.textSize
        }

        Text {
            id: rightDist
            textFormat: Text.RichText
            text: qsTr("Z<sub>R</sub>:")
            font.pointSize: parent.textSize
        }

        Text {
            id: rightDistValue
            text: "0"
            font.pointSize: parent.textSize
        }
    }

    Text {
        id: notificationMessage
        font.pixelSize: 22
        anchors { top: parent.verticalCenter; horizontalCenter: parent.horizontalCenter; }
        text: internalVars.initialMessage
        textFormat: Text.RichText

        onTextChanged: messageTimer.stop();

        NumberAnimation on opacity {
            id: notificationAnimation
            duration: 700
            easing.type: Easing.InOutExpo
            from: 0
            to: 1
        }

        Timer {
            id: messageTimer
            interval: 700
            repeat: true

            onTriggered: {
                notificationAnimation.start();
            }
        }
    }

    Row {
        id: eyeImagesRow
        anchors { bottom: parent.bottom; right: parent.right; bottomMargin: 50; rightMargin: 20; }

        property size imageSize: "200x200"
        property string leftEyeImage1: "image://eyesprovider/lefteye1"
        property string rightEyeImage1: "image://eyesprovider/righteye1"
        property string leftEyeImage2: "image://eyesprovider/lefteye2"
        property string rightEyeImage2: "image://eyesprovider/righteye2"
        visible: false

        function refreshImages() {
            leftEye1.source = "";
            leftEye1.source = leftEyeImage1;

            rightEye1.source = "";
            rightEye1.source = rightEyeImage1;

            leftEye2.source = "";
            leftEye2.source = leftEyeImage2;

            rightEye2.source = "";
            rightEye2.source = rightEyeImage2;
        }

        Image {
            id: leftEye1
            source: parent.leftEyeImage1
            width: parent.imageSize.width
            height: parent.imageSize.height
            sourceSize: parent.imageSize
            fillMode: Image.PreserveAspectFit
            cache: false
        }

        Image {
            id: rightEye1
            source: parent.rightEyeImage1
            width: parent.imageSize.width
            height: parent.imageSize.height
            sourceSize: parent.imageSize
            fillMode: Image.PreserveAspectFit
            cache: false
        }

        Image {
            id: leftEye2
            source: parent.leftEyeImage2
            width: parent.imageSize.width
            height: parent.imageSize.height
            sourceSize: parent.imageSize
            fillMode: Image.PreserveAspectFit
            cache: false
        }

        Image {
            id: rightEye2
            source: parent.rightEyeImage2
            width: parent.imageSize.width
            height: parent.imageSize.height
            sourceSize: parent.imageSize
            fillMode: Image.PreserveAspectFit
            cache: false
        }
    }

    MouseArea {
        id: gestureDetectMouseArea

        anchors.fill: parent
        propagateComposedEvents: true

        property point startPos
        property point endPos
        property int threshold: 150
        property int showEyesThreshold: 200

        onPressed: {
            startPos = Qt.point(mouseX, mouseY);
            mouse.accepted = false;
            if (startPos.x < threshold && startPos.y < threshold)
                mouse.accepted = true;
            else if (startPos.y > height - 50 - showEyesThreshold && startPos.y < height - 50 && startPos.x < threshold)
                mouse.accepted = true;
        }

        onReleased: {
            endPos = Qt.point(mouseX, mouseY);
            if (startPos.x < threshold && startPos.y < threshold &&
                    endPos.x > width - threshold && endPos.y > height - threshold) {
                debugView.visible = true;
            } else if (startPos.y > height - 50 - showEyesThreshold && startPos.y < height - 50 && startPos.x < threshold
                       && endPos.x > width - threshold && endPos.y > height - 50 - showEyesThreshold && endPos.y < height - 50) {
                eyeImagesRow.visible = !eyeImagesRow.visible;
            }
        }
    }

    Connections {
        target: videoController

        onDetectionFailed: {
            notificationMessage.text = internalVars.detectionFailedMessage
            notificationMessage.visible = true;
            showInitialMessageTimer.start();
            console.log("MainView.qml: detection failed");
        }

        onMeasurementFinished: {
            var pd = Math.sqrt(Math.pow(leftEye.x - rightEye.x, 2.0) +
                               Math.pow(leftEye.y - rightEye.y, 2.0) +
                               Math.pow(leftEye.z - rightEye.z, 2.0));
            var avgDist = (leftEye.z + rightEye.z) / 2;
            var correctedPd = pd * (avgDist + 14) / avgDist;

            pdValue.text = correctedPd.toFixed(1);

            leftDistValue.text = leftEye.z.toFixed(1);
            rightDistValue.text = rightEye.z.toFixed(1);

            console.log(imageIndex + "," + pd + "," +leftEye.z.toFixed(1) +","  +rightEye.z.toFixed(1))

            infoGrid.visible = true;
            //            console.log("MainView.qml: computed pd: " + pd);
            notificationMessage.visible = false;
            showInitialMessageTimer.start();
            eyeImagesRow.refreshImages();
        }
    }

    Timer {
        id: showInitialMessageTimer
        interval: 20000

        onTriggered: {
            notificationMessage.text = internalVars.initialMessage;
            notificationMessage.visible = true;
            internalVars.measurementEnabled = true;
        }
    }

    PDScannerMain {
        id: debugView
        anchors { top: parent.top; left: parent.left; }
        visible: false

        onVisibleChanged: {
            videoController.setDebugMode(visible);
        }
    }
}
