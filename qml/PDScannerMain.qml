import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Controls.Styles 1.0

import "../js/Constants.js" as Constants

Rectangle {
    id: root
    width: 400
    height: 400

    color: "lightgrey"

    QtObject {
        id: privateMembers
        property bool measurementEnabled: true
        property bool showCapturedImage: false
        property real pupillaryDistance: 0
        property vector3d leftEye: Qt.vector3d(0, 0, 0)
        property vector3d rightEye: Qt.vector3d(0, 0, 0)
    }

    ImageButton {
        id: closeButton
        image: Constants.CloseIcon
        pressedImage: Constants.CloseIcon
        width: 50
        height: 50
        anchors { top: parent.top; left: parent.left; }

        onClicked: root.visible = false;
    }

    ImageButton {
        id: shutdownButton
        image: Constants.ShutdownIcon
        pressedImage: Constants.ShutdownIcon
        width: 50
        height: 50
        anchors { top: parent.top; right: parent.right; }

        onClicked: {
            sysUtils.shutdown();
        }
    }

    RowLayout {
        anchors { horizontalCenter: parent.horizontalCenter; bottom: parent.bottom; bottomMargin: 10; }
        spacing: 25

        ColumnLayout{

            spacing: 10

            Button{
                id: startMeasurementButton


                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "Start"
                width:  resultTextArea.width
                height: 100
                Layout.minimumHeight: 100
                enabled: privateMembers.measurementEnabled

                onClicked: {
                    resultTextArea.text = ""

                    if(videoController.startNewMeasurement())
                        privateMembers.measurementEnabled = false;
                }

            }

            Button{

                text: "Move Cams Up"
                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: videoController.moveCamerasToTop()
                width:  resultTextArea.width
                height: 30
            }

            Button{
                text: "Stop measurement"

                Layout.fillHeight: true
                Layout.fillWidth: true
                onClicked: videoController.stopMeasurement()
                width: resultTextArea.width
                height: 30
            }

            TextArea{
                id: resultTextArea

                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }

        ColumnLayout{
            id: cameraButtons

            spacing: 10

            Layout.alignment: Qt.AlignVCenter

            AutorepeatButton{
                //            Button{


                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "Move Up"
                width: 100
                height: 40
                autoRepeat: true
                onButtonPressed:{
                    //                onClicked: {
                    videoController.moveCamerasSingle(true);
                    privateMembers.measurementEnabled = true;

                }

                onButtonReleased: {
                    videoController.stopTowerMove();
                }
            }

            //            Button{
            AutorepeatButton{
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "Move Down"
                width: 100
                height: 40

                onButtonPressed: {
                    //                onClicked: {
                    videoController.moveCamerasSingle(false);
                    privateMembers.measurementEnabled = true;
                }

                onButtonReleased: {
                    videoController.stopTowerMove();
                }
            }


            Button{
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "Stop Move"
                width: 100

                onClicked: {
                    videoController.stopTowerMove();
                    privateMembers.measurementEnabled = true;
                }
            }

            Button{
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "Lights On"
                width: 100

                onClicked: {
                    videoController.setLights(true);
                    privateMembers.measurementEnabled = true
                }
            }

            Button{
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "Lights Off"
                width: 100

                onClicked: {
                    videoController.setLights(false);
                    privateMembers.measurementEnabled = true
                }
            }

            Button{
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: "Capture image"
                width: 100

                onClicked: {
                    if(!videoController.captureStereoImage())
                        console.log("Error while capturing image");
                    privateMembers.measurementEnabled = true
                }
            }
        }
    }

    Connections{
        target: videoController
        onEyesDetected:{
            privateMembers.measurementEnabled = true;
        }

        onDetectionFailed:{
            privateMembers.measurementEnabled = true;
        }

        onMeasurementFinished:{
            console.log(leftEye, rightEye)

            privateMembers.leftEye = leftEye
            privateMembers.rightEye = rightEye

            privateMembers.pupillaryDistance = Math.sqrt(Math.pow(leftEye.x - rightEye.x, 2.0)+
                                                         Math.pow(leftEye.y - rightEye.y, 2.0)+
                                                         Math.pow(leftEye.z - rightEye.z, 2.0));

            resultTextArea.append("left eye: "+privateMembers.leftEye)
            resultTextArea.append("rightEye "+privateMembers.rightEye)
            resultTextArea.append("PUPILLARY DISTANCE "+privateMembers.pupillaryDistance)
        }
    }
}
