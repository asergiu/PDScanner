import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle{
    id:btn

    border.color: "black"
    border.width: 1
    radius: 4
    property string color_disabled: "#B4B5B5"
    property string color_kept_pressed: "#E0E0F0"
    property string color_normal: "white"
    property alias text: buttonText.text
    color: btn.disabled ? color_disabled: ( timer_btn.running ? color_kept_pressed :  color_normal)

    property bool autoRepeat: true
    property int autoRepeatInterval: 100
    property bool disabled: false

    signal buttonPressed()
    signal buttonReleased()
    signal buttonClicked()


    Text{
        id: buttonText

        anchors.centerIn: parent
        color: "black"
    }

    MouseArea{
        id: btn_mousearea
        anchors.fill: parent
        onPressed: {
            buttonPressed()
        }

        onPressAndHold: {
            if(!autoRepeat || timer_btn.running) return;
            timer_btn.start();
        }
        onReleased: {
            if (timer_btn.running) timer_btn.stop()
            buttonReleased()
//            buttonClicked()
        }
    }

    Timer{
        id: timer_btn
        repeat: btn.autoRepeat
        interval: btn.autoRepeatInterval
        triggeredOnStart: true
        onTriggered: buttonPressed()
    }
}
