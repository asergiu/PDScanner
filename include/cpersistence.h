#ifndef CPERSISTENCE_H
#define CPERSISTENCE_H

#include "utils_global.h"

#include <QHash>
#include <QDomDocument>
#include <QDomNode>
#include <QDomElement>

class CPersistentBag;

class CORESHARED_EXPORT CPersistence {
public:
    static CPersistence* getInstance(QString xmlFile, QString rootTag = "persistence");
    ~CPersistence();
    
    static CPersistentBag getXMLPersistentBag(QString xmlFile, QString name, QString id = "");
    CPersistentBag getPersistentBag(QString name, QString id = "");
    bool deleteBag(QString name, QString id = "");
    bool deleteBag(CPersistentBag bag);

    QList<CPersistentBag> bagList();

    bool save();
    bool saveAs(QString xmlFileName);

    bool isEmpty();

    bool reload(QString rootTag = "persistence");

protected:
    explicit CPersistence(QString rootTag = "persistence");
    bool load(QString xmlFileName = "");

//signals:
    
//public slots:

private:
    static QHash<QString, CPersistence*> m_persistanceHash;

    //! returns the main DomDocument and also initializes it if needed
    QDomDocument*   document(QString rootTag = "persistence");

    //! return the node/element with tagName==name and attribute ID=id ; if there's no such node returns an empty node
    QDomElement     getElement(QString name, QString id);


    QString         m_filename;
    QDomDocument*   m_document;
};

#endif // CPERSISTENCE_H
