#ifndef SYSTEMUTILS_H
#define SYSTEMUTILS_H

#include <QObject>
#include <QDateTime>
#include "utils_global.h"
#include <QProcess>
#include <QNetworkReply>
#include <QUrl>
#include <QVariant>
#include <QVariant>
#include <QByteArray>
#include <QNetworkAccessManager>
#include <QQuickView>

class ScreenSaverController;

class CORESHARED_EXPORT SystemUtils : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int processStatus READ processStatus NOTIFY processStatusChanged)

public:
    explicit SystemUtils(QObject *parent = 0);

    Q_INVOKABLE int runProcess(QString command);
    Q_INVOKABLE void launchProcess(QString command, QString workingDir);
    Q_INVOKABLE bool concatImages(QString images,  QString imageName, int nrRows = 1, int nrCols = 1);
    Q_INVOKABLE bool deleteFile(QString path);
    Q_INVOKABLE bool fileExists(QString path);
    Q_INVOKABLE bool copyFile(QString src, QString dest);

    const QString readFile(const QString &fileName) const;
    QImage concatImages(QList<QImage> images, QString imageName, int nrRows = 1, int nrCols = 1);

    QVariantList toVariantList(const QPolygonF &list);
    QPolygonF fromVariantList(const QVariantList &list, bool closed);

    QNetworkReply *makeSyncRequest(QUrl url);
    QVariant getJsonValue(QByteArray json, QString key);

    Q_INVOKABLE QString buildVersion();
    Q_INVOKABLE QString buildVersionClean();
    Q_INVOKABLE QString buildBranch();
    Q_INVOKABLE int buildRevisionCount();
    Q_INVOKABLE QString buildDate();

    Q_INVOKABLE QString ipAddress();
    Q_INVOKABLE QString ipAddressAsUList();
    Q_INVOKABLE QString serviceTag();
    Q_INVOKABLE QString hwInfo(QString component, QString item);

    Q_INVOKABLE void restartX();
    Q_INVOKABLE void reboot();
    Q_INVOKABLE void shutdown();

    Q_INVOKABLE QVariant getConfigXMLTagValue(QString tag);
    Q_INVOKABLE void setConfigXMLTagValue(QString value, QString tag);
    ~SystemUtils();

    static QString CONFIG_FILE;

public slots:

    int processStatus(){ return m_processStatus; }

public slots:
    void onProcessFinished(int exitCode);
    void onProcessError(QProcess::ProcessError err);

signals:
    void processStatusChanged();

private:
    QImage concatImages(QList<QImage> images, int nrRows, int nrCols);
    int m_processStatus;
    QProcess* m_proc;
    QNetworkAccessManager *m_networkManager;
};

#endif // SYSTEMUTILS_H
