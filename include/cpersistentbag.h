#ifndef CPERSISTENTBAG_H
#define CPERSISTENTBAG_H

#include "utils_global.h"

#include <QDomElement>
#include <QVariant>
#include <QStringList>

class CPersistence;

class CORESHARED_EXPORT CPersistentBag : public QDomElement {

    friend class CVideoPersistence;

public:
    explicit CPersistentBag(CPersistence* persistence = NULL);
    CPersistentBag(QDomElement &el,CPersistence* persistence = NULL );

    void setPersistence(CPersistence* persitence);
    CPersistence* persistence();


    QString name()              { return this->tagName();       }
    void setName(QString name)  { setTagName(name);             }
    QString id()                { return attribute("ID", "");   }
    void setID(QString newID)   { setAttribute("ID", newID);    }
    QString type()              { return attribute("TYPE", ""); }

    QVariant getPropertyValue(QString name, QString ID = "");
    bool setPropertyValue(QString name,  QVariant value, QString ID = "");
    bool readStringValue(QString propertyName, QString &var);
    bool readIntValue(QString propertyName, int &var);

    CPersistentBag getPropertyBag(int index);
    CPersistentBag getPropertyBag(QString name, QString ID = "");

    int propertyCount();
    QVariant getPropertyValue(int index);
    const QStringList propertyNameList();

    QVariant value(){ return elementValue(this->toElement()); }
    void setValue(QVariant value){ return setElementValue(this->toElement(), value); }

protected:
    QVariant elementValue(QDomElement el);
    void setElementValue(QDomElement el, QVariant value);
    QDomElement getElement(QString name, QString id = "");

signals:
    
public slots:

protected:
    // private as we want to automatically set type from QVariant.typeName / code
    void setType(QString type)  { setAttribute("TYPE", type);   }


    CPersistence *m_persistence;

};

#endif // CPERSISTENTBAG_H
