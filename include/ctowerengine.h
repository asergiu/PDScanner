#ifndef CTOWERENGINE_H
#define CTOWERENGINE_H

#include "utils_global.h"

#include <QObject>
#include <QTimer>
#include <QDateTime>


class CORESHARED_EXPORT CTowerEngine : public QObject {
    Q_OBJECT

    Q_PROPERTY(LightPorts port READ port WRITE setPort)
    Q_PROPERTY(bool light READ light NOTIFY lightChanged)
    Q_PROPERTY(bool lightSmart READ lightSmart NOTIFY lightSmartChanged)
    Q_PROPERTY(bool busy READ isBusy NOTIFY busyChanged)
    Q_PROPERTY(bool error READ isError NOTIFY errorChanged)
    Q_PROPERTY(bool onlyFrameSelection READ onlyFrameSelection WRITE setOnlyFrameSelection)

    Q_ENUMS(LighPorts)
public:

    enum LightPorts { COM1 = 0x03f8, COM2 = 0x02f8 };

    CTowerEngine( LightPorts port = COM2 );
    ~CTowerEngine();

    Q_INVOKABLE void moveUp();
    Q_INVOKABLE void moveDown();
    Q_INVOKABLE void stop();

    Q_INVOKABLE void turnLights( bool on_off ); // turns the light ON / OFF
    Q_INVOKABLE void turnLightsSmart( bool on_off );
    Q_INVOKABLE void toggleLights();
    Q_INVOKABLE void toggleLightsSmart();

    inline bool light() const { return  m_light; }
    inline bool lightSmart() const { return  m_light2; }
    inline bool isBusy()const { return  m_isbusy; }
    inline bool isError()const { return  m_isError; }
    inline bool onlyFrameSelection()const { return  m_onlyFrameSelection; }
    void setOnlyFrameSelection(bool value){ m_onlyFrameSelection = value; }

    LightPorts port() { return m_port; }
    void setPort( LightPorts );
    void initPort();
    void closePort();

signals:
    void portChanged();
    void lightChanged();
    void lightSmartChanged();
    void busyChanged();
    void errorChanged();

public slots:
    virtual void onLightTimer();


private:
    void doCommand( char command );

    LightPorts  m_port;
    bool        m_isbusy;
    bool        m_isError;
    bool        m_light;
    bool        m_light2;
    int         m_fd;
    QTimer      m_timerLight;
    QTime       m_elapsedTime;
    bool        m_onlyFrameSelection;
};



#endif // CTOWERENGINE_H
