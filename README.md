**E-Column PD Measurement Module**

This is the e-Column module for automatic PD measurement with the e-Column hardware conceived in cooperation betwen ACEP and TVARITA
within the financed project: _Development of high precision, fixed and mobile sterescopic high resolution measurement tools in optical medicine_
