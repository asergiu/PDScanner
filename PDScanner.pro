DESTDIR = deploy
OBJECTS_DIR = obj
MOC_DIR = gen
RCC_DIR = gen

QT += testlib widgets

RESOURCES += PDScanner.qrc

SOURCES += main.cpp \
    cpp/cstereosystem.cpp \
    cpp/cvideocapture.cpp \
    cpp/cvideocontroller.cpp \
    cpp/cdetection.cpp \
    cpp/pdscannerinitializer.cpp \
    cpp/cstereodetection.cpp \
    cpp/eyesimageprovider.cpp \
    cpp/cconversionutils.cpp

HEADERS += \
    cpp/opencvheaders.h \
    cpp/cstereosystem.h \
    cpp/cvideocapture.h \
    cpp/cvideocontroller.h \
    cpp/cdetection.h \
    cpp/types.h \
    cpp/pdscannerinitializer.h \
    cpp/debug.h \
    cpp/cstereodetection.h \
    cpp/eyesimageprovider.h \
    cpp/cconversionutils.h

OTHER_FILES += \
    qml/PDScannerMain.qml \
    qml/AutorepeatButton.qml \
    qml/ScanningBar.qml \
    qml/MainView.qml \
    js/Constants.js \
    qml/ImageButton.qml


# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

############ opencv library #############################
unix {
    INCLUDEPATH += /usr/local/opencv243/include
    LIBS += -L/usr/local/opencv243/lib -lopencv_core -lopencv_objdetect -lopencv_objdetect -lopencv_calib3d -lopencv_imgproc -lopencv_highgui -lopencv_nonfree -lopencv_features2d
}

######### utils library #################################
unix {
    INCLUDEPATH += include
    LIBS += -Ldeploy/lib -lutils

    LIBS += -L/usr/local/qwt-6.1.1/lib
    unix: LIBS += -lqwt
}
