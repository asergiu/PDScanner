#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <QCoreApplication>
#include <QtQml>
#include <QQmlEngine>
#include <QApplication>
#include <QDesktopWidget>

#include "qtquick2applicationviewer.h"
#include "cpp/pdscannerinitializer.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QtQuick2ApplicationViewer viewer;

    PDScannerInitializer initializer;
    initializer.initApplication(viewer);

    viewer.setSource(QUrl("qrc:/qml/MainView.qml"));

    int minWidth = 1023, minHeight = 767;
    viewer.setWidth(minWidth);
    viewer.setHeight(minHeight);
    viewer.setMinimumSize(QSize(minWidth, minHeight));

    viewer.setFlags(Qt::FramelessWindowHint);
    viewer.showExpanded();

    return app.exec();
}
