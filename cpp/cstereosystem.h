#ifndef CSTEREOSYSTEM_H
#define CSTEREOSYSTEM_H

#include <QSize>
#include <QPointF>
#include <QObject>
#include <QDebug>
#include <QVector3D>
#include <QStringList>
#include "opencvheaders.h"


class CStereoSystem : public QObject
{
    Q_OBJECT
public:
    explicit CStereoSystem(QObject *parent = 0);

    Q_INVOKABLE void clearCalibrationData();
    Q_INVOKABLE float calibrateAndRectify(const QString imagesPath, QSize boardSize, const float squareSize = 1.0f, bool displayCorners = true, bool showRectified = false, bool transpose = false);
    Q_INVOKABLE bool rectifyImages(QString leftImage, QString rightImage, QString leftRectified = QString(), QString rightRectified = QString(), bool tranpose = false);
    Q_INVOKABLE bool rectifyImagesColor(QString leftImage, QString rightImage, QString leftRectified, QString rightRectified, bool transpose = false);
    Q_INVOKABLE bool saveIntrinsicsParams(QString intrinsicsFile, int mode = CV_STORAGE_WRITE);
    Q_INVOKABLE bool loadIntrinsicsParams(QString intrinsicsFile);

    Q_INVOKABLE QVector3D compute3DPosition(QPointF qimagePoint1, QPointF qimagePoint2);

    Q_INVOKABLE bool isVerticalStereo();
    Q_INVOKABLE void setStereoParamsFile(QString fileName);
    Q_INVOKABLE bool stereoSystemInitialized();

    Mat getRectififedImage(int imageIndex);

    bool loadCalibrationImages(const QString fileName, QStringList &l);
    bool saveExtrinsicParameters(QString extrinsicsFile, int mode = CV_STORAGE_WRITE);
    bool loadExtrinsicsParams(QString extrinsicsFile);

    bool saveStereoSystemParams(QString file = QString("stereoSystemParam.yml"));
    bool loadStereoSystemParams(QString file);

    Point3d compute3DPositionUndistoredPoints(Point2d pointLeftImage, Point2d pointRightImage);
    Point3d compute3DPosition(Point2d pointLeftImage, Point2d pointRightImage);
    bool rectifyImages(Mat leftImageMat, Mat rightImageMat, bool transpose = false);


signals:

public slots:

private:

    Mat m_Q, m_T, m_R, m_E, m_F;
    Mat m_R1, m_R2, m_P1, m_P2;
    Mat m_cameraMatrix[2];
    Mat m_distCoeffs[2];
    Rect m_validRoi[2];
    Mat m_rmap[2][2];
    bool m_isVerticalStereo;
    Size m_imageSize;

    std::vector<std::string> goodImageList;
    cv::Mat m_leftRectifiedImage, m_rightRectifiedImage;

    QString m_stereoParamsFile;
    static const QString IMAGES_XML;

    bool setRoi(const Mat &image, cv::Rect roiRect, cv::Mat &roiMat);
    bool enhanceColorImage(Mat inputImage,  Mat &result);

};

#endif // CSTEREOSYSTEM_H
