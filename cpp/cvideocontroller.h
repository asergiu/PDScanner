#ifndef CVIDEOCONTROLLER_H
#define CVIDEOCONTROLLER_H

#include <QObject>
#include <QTimer>
#include <QRectF>
#include <QString>
#include <QVector3D>


#include "types.h"
#include "opencvheaders.h"
#include "systemutils.h"

class CVideoCapture;
class CTowerEngine;
class CStereoSystem;
class CDetection;
class EyesImageProvider;

class CVideoController : public QObject
{
    Q_OBJECT
public:
    explicit CVideoController(QObject *parent = 0);
    ~CVideoController();

    Q_INVOKABLE bool startNewMeasurement();
    Q_INVOKABLE void stopMeasurement();
    Q_INVOKABLE void moveCamerasToTop();
    Q_INVOKABLE void moveCamerasSingle(bool moveUp);
    Q_INVOKABLE void setLights(bool onOff);
    Q_INVOKABLE bool captureStereoImage(QString leftImageName = QString(), QString rightImageName = QString());
    Q_INVOKABLE void stopTowerMove();
    Q_INVOKABLE void setDebugMode(bool debug);

    void setEyesImageProvider(EyesImageProvider *imageProvider);

signals:
    void newStereoCameraFrame(cv::Mat leftImage, cv::Mat rightImage);
    void capturedImage(QString imageName);

    void eyesDetected();
    void detectionFailed();
    void measurementFinished(int imageIndex, QVector3D leftEye, QVector3D rightEye);


public slots:
    void processNewStereoCameraFrame(Mat leftImage, Mat rightImage);
    void onDetectionResult(bool detected, QRectF faceLeftImg, QPointF leftEyeLeftImg, QPointF rightEyeLeftImg, QRectF leftEyeRectLeftImg, QRectF rightEyeRectLeftImg, QRectF faceRightImg, QPointF leftEyeRightImg, QPointF rightEyeRightImg, QRectF leftEyeRectRightImg, QRectF rightEyeRectRightImg);
    void onTowerStopMoving();
    void stopEyesDetection();

private:

    void rescaleImage();

    typedef enum {
        MOVING_UP = 0,
        MOVING_DOWN
    } movingDirection;

    EyesImageProvider *m_eyesImageProvider;
    CStereoSystem* m_stereoSystem;
    CVideoCapture* m_videoCapture;
    CDetection* m_detectionThread;
    CTowerEngine* m_towerEngine;
    bool m_performDetection, m_stopDetection;
    detectionType m_detectionType;
    int m_closestCameraToLight;
    bool m_saveImages;

    // the direction in which the tower is moving
    movingDirection m_movingDirection;
    double m_moveDuration;

    bool m_isDebugMode;
    SystemUtils sysUtils;

    QTimer m_moveDurationTimer;
    QSize m_imageSize;

    int m_eyesDetectionFrames;

    // time needed for the tower to move the cameras
    // from the top to the bottom (in seconds)
    static const double TOTAL_TOWER_MOVE_DURATION_MSEC;

    // the maximum time the tower is moving after moveUp()
    // or moveDown() commands are invoked
    static const double TOWER_MOVE_DURATION_MSEC;

    static const QString STEREO_SYSTEM_PARAMS;

    static const std::string CAPTURE_WINDOW1;
    static const std::string CAPTURE_WINDOW2;
    static const std::string EYES_WINDOW1;
    static const std::string EYES_WINDOW2;

    bool mustStopTower(QPointF &leftEyeLeftImg, QPointF &rightEyeLftImg, QPointF &leftEyeRightImg, QPointF &rightEyeRightImg);
    void setImageNames(std::string leftImageBase, std::string rightImageBase, int index, std::string &leftImageName, std::string &rightImageName);
};

#endif // CVIDEOCONTROLLER_H
