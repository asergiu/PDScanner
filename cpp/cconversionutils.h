#ifndef CCONVERSIONUTILS_H
#define CCONVERSIONUTILS_H

#include "opencvheaders.h"
#include <QPointF>
#include <QImage>
#include <QVector3D>
#include <QRectF>

class CConversionUtils
{
public:
    CConversionUtils();

    static Rect qRectToRect(QRectF qRectangle);
    static QRectF rectToQRectF(Rect cvRect);

    static Point2d qPointToPoint(QPointF qPoint);
    static QPointF pointToQPointF(Point2d cvPoint);

    static QVector3D point3DToQVector(cv::Point3d cvPoint);

    static QImage mat2QImage(cv::Mat const& src);

    static cv::Mat qImage2Mat(QImage const& src);

    static bool setRoi(const Mat &image, cv::Rect roiRect, cv::Mat &roiMat);

    static Point2d zoneGravityCenter(std::vector<Point> seqContour);

    static bool interesct(Rect r1, Rect r2);
    static Rect maxRect(Rect r1, Rect r2);
};

#endif // CCONVERSIONUTILS_H
