#ifndef EYESIMAGEPROVIDER_H
#define EYESIMAGEPROVIDER_H

#include <QObject>
#include <QQuickImageProvider>

class EyesImageProvider : public QObject, public QQuickImageProvider
{
    Q_OBJECT
public:
    explicit EyesImageProvider(QQuickImageProvider::ImageType type);

    QImage requestImage(const QString& id, QSize* size, const QSize &requestedSize);
    QPixmap requestPixmap(const QString& id, QSize* size, const QSize& requestedSize);

    Q_INVOKABLE void setLeftEyeImage1(QImage img);
    Q_INVOKABLE void setRightEyeImage1(QImage img);
    Q_INVOKABLE void setLeftEyeImage2(QImage img);
    Q_INVOKABLE void setRightEyeImage2(QImage img);

private:
    QImage m_leftEye1;
    QImage m_rightEye1;
    QImage m_leftEye2;
    QImage m_rightEye2;
};

#endif // EYESIMAGEPROVIDER_H
