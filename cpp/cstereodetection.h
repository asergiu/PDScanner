#ifndef CSTEREODETECTION_H
#define CSTEREODETECTION_H

#include "opencvheaders.h"

#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;

class CStereoDetection
{
public:

    typedef enum{
        LEFT_IMAGE = 0,
        RIGHT_IMAGE,
        NUM_IMAGES
    } ImageType;

    enum EYE_POINT_TYPE {
        EYE_POINT_RIGHT = 0,        /**< The right eye. **/
        EYE_POINT_LEFT,             /**< The left eye **/
        MAX_EYE_POINTS              /**< The total number of eye types. **/
    };

    CStereoDetection();

    bool initDetection(std::string cascadeClassifiersPath);
    bool isDetectionInitialized();
    bool loadImages(cv::Mat &leftImage, cv::Mat &rightImage);
    bool areImagesLoaded();
    bool detectFaces();


    bool detectEyes();
    bool detectCornealReflection();


    void clearDetectionData();
    bool facesDetected();
    bool eyesDetected();

    cv::Point2d getCornealReflection(EYE_POINT_TYPE eyeType, ImageType imageType);
    cv::Rect getEyesRectangle(EYE_POINT_TYPE eyeType, ImageType imageType);
    cv::Point2d getEyesCenter(EYE_POINT_TYPE eyeType, ImageType imageType);
    cv::Rect getFaceRectangle(ImageType imageType);

    bool hasCornealReflection(EYE_POINT_TYPE eyeType, ImageType imageType);
    bool hasEyeDetected(EYE_POINT_TYPE eyeType, ImageType imageType);
    bool hasFaceDetected(ImageType imageType);

    bool detectCornealReflectionBlob();
private:
    cv::Mat m_image[NUM_IMAGES];
    cv::Rect m_faceRect[NUM_IMAGES];
    cv::Rect m_eyesRect[MAX_EYE_POINTS][NUM_IMAGES];
    cv::Point2d m_cornealReflection[MAX_EYE_POINTS][NUM_IMAGES];

    static const cv::Size FACE_DETECTION_IMAGE_SZ;

    cv::CascadeClassifier m_faceCascade, m_eyesCascade;

    bool detectFace(const cv::Mat &inputImage, cv::Rect &faceRectangle);
    bool detectEyes(cv::Mat inputImage,  cv::Rect &leftEye, cv::Rect & rightEye);
    std::pair<cv::Point2d, cv::Point2d> findAndMatchCorners(cv::Mat leftImage, cv::Mat rightImage, std::vector<Rect> &leftImgContours, std::vector<Rect> &rightImgContours);
    std::vector<double> evaluateCornealReflectionCandidates(Mat &inputIMage, std::vector<Point2d> candidates);
    std::vector<cv::Rect> detectCornealReflectionMorphology(Mat inputImage);

    std::vector<double> evaluateCornealReflectionCandidates(Mat &inputImage, std::vector<KeyPoint> candidates);
    cv::Point2d centerCornealReflection(Mat inputImage, cv::Point2d originalReflection);
};

#endif // CSTEREODETECTION_H
