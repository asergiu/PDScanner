#include "cconversionutils.h"

CConversionUtils::CConversionUtils()
{
}

Rect CConversionUtils::qRectToRect(QRectF qRectangle){
    if(qRectangle == QRectF())
        return Rect();

    return Rect(qRectangle.x(), qRectangle.y(), qRectangle.width(), qRectangle.height());
}

QRectF CConversionUtils::rectToQRectF(Rect cvRect){
    if(cvRect == Rect())
        return QRectF();

    return QRectF(cvRect.x, cvRect.y, cvRect.width, cvRect.height);
}

Point2d CConversionUtils::qPointToPoint(QPointF qPoint){
    if(qPoint == QPointF())
        return Point2d();

    return Point2d(qPoint.x(), qPoint.y());
}

QPointF CConversionUtils::pointToQPointF(Point2d cvPoint){
    if(cvPoint == Point2d())
        return QPointF();

    return QPointF(cvPoint.x, cvPoint.y);
}

QVector3D CConversionUtils::point3DToQVector(Point3d cvPoint){
    if(cvPoint == Point3d())
        return QVector3D();

    return QVector3D(cvPoint.x, cvPoint.y, cvPoint.z);
}

QImage CConversionUtils::mat2QImage(const Mat &src)
{
    cv::Mat temp; // make the same cv::Mat
    cvtColor(src, temp,CV_BGR2RGB); // cvtColor Makes a copt, that what i need
    QImage dest((uchar*) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
    QImage dest2(dest);
    dest2.detach(); // enforce deep copy
    return dest2;
}

Mat CConversionUtils::qImage2Mat(const QImage &src)
{
    cv::Mat tmp(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine());
    cv::Mat result;
    cvtColor(tmp, result,CV_BGR2RGB);
    return result;
}

bool CConversionUtils::setRoi(const Mat &image, cv::Rect roiRect, cv::Mat &roiMat){


    roiMat = Mat(roiRect.size(), CV_8UC3);
    roiMat.setTo(0);

    Rect inImageRect = roiRect;
    if(inImageRect.x < 0)
        inImageRect.x = 0;
    if(inImageRect.y < 0)
        inImageRect.y = 0;

    if(inImageRect.x + inImageRect.width > image.cols)
        inImageRect.width = image.cols - inImageRect.x;

    if(inImageRect.y + inImageRect.height > image.rows)
        inImageRect.height = image.rows - inImageRect.y;


    Mat originalImageRoi = Mat(image, inImageRect);


    copyMakeBorder( originalImageRoi, roiMat, inImageRect.y - roiRect.y, roiRect.height - inImageRect.height, inImageRect.x - roiRect.x, roiRect.width - inImageRect.width, BORDER_CONSTANT, 0 );


    //    namedWindow("originalImage", WINDOW_NORMAL);
    //    imshow("originalImage", image);

    //    namedWindow("origroi");
    //    imshow("origroi", originalImageRoi);

    //    namedWindow("roi", WINDOW_NORMAL);
    //    imshow("roi", roiMat);
    //    waitKey(0);

    return true;

}

Point2d CConversionUtils::zoneGravityCenter(std::vector<Point> seqContour){
    Point2d maxLoc = Point2d(0, 0);
    Moments moments;

    //Compute Gravity center from moments:
    moments = cv::moments(seqContour);

    if(moments.m00 != 0){
        maxLoc.x = moments.m10 / moments.m00;
        maxLoc.y = moments.m01 / moments.m00;
    }

    return maxLoc;
}

bool CConversionUtils::interesct(Rect r1, Rect r2){

    if(r1 == Rect() || r2 == Rect())
        return false;

    Rect intersectionRect = r1 & r2;

    return (intersectionRect != Rect());
}

Rect CConversionUtils::maxRect(Rect r1, Rect r2){
    return (r1 & r2) == r1 ? r2 : r1;
}

