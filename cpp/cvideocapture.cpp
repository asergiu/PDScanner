#include "cvideocapture.h"
#include "debug.h"

#ifdef SHOW_EXECUTION_TIME
#include <time.h>
#endif

CVideoCapture::CVideoCapture() :
    QThread()
{
    m_cameraSize = Size();
    m_captureFrames = true;
    m_isStereoCamera = false;
}

CVideoCapture::~CVideoCapture()
{
//    std::cerr<<"~CVideoCapture()"<<std::endl;
    closeCameras();

    m_camera1Image.release();
    m_camera2Image.release();
}

bool CVideoCapture::openCamera(int cameraIndex, Size cameraSize, bool showPreview)
{
    m_isStereoCamera = false;
    if(m_videoCap1.isOpened())
        m_videoCap1.release();
    if(m_videoCap2.isOpened())
        m_videoCap2.release();

    m_videoCap1 = VideoCapture(cameraIndex);

    m_videoCap1.set(CV_CAP_PROP_FRAME_WIDTH, cameraSize.width);
    m_videoCap1.set(CV_CAP_PROP_FRAME_HEIGHT, cameraSize.height);

    Size openedCameraSize = cv::Size(m_videoCap1.get(CV_CAP_PROP_FRAME_WIDTH), m_videoCap1.get(CV_CAP_PROP_FRAME_HEIGHT));

    if(!m_videoCap1.isOpened() || openedCameraSize == Size() || openedCameraSize == Size(0, 0)){
//        std::cerr<<"Could not open camera!!!"<<std::endl;
        return false;
    }

    std::cerr<<"Camera resolution: "<<openedCameraSize.width<<" "<<openedCameraSize.height<<std::endl;
    m_cameraSize = openedCameraSize;

    if(!showPreview) return true;

    Mat cameraFrame;

    for(;;){
        // get new frame from camera
        m_videoCap1 >> cameraFrame;

        namedWindow("cameraPreview", WINDOW_NORMAL);
        imshow("cameraPreview", cameraFrame);

        if(waitKey(30) >= 0) break;
    }

    return true;
}

bool CVideoCapture::openCameras(int cam1Index, int cam2Index, Size cameraSize, bool showPreviews)
{
    m_isStereoCamera = true;
    if(m_videoCap1.isOpened())
        m_videoCap1.release();

    if(m_videoCap2.isOpened())
        m_videoCap2.release();

    m_videoCap1 = VideoCapture(cam1Index);
    m_videoCap2 = VideoCapture(cam2Index);

    m_videoCap1.set(CV_CAP_PROP_FRAME_WIDTH, cameraSize.width);
    m_videoCap1.set(CV_CAP_PROP_FRAME_HEIGHT, cameraSize.height);

    m_videoCap2.set(CV_CAP_PROP_FRAME_WIDTH, cameraSize.width);
    m_videoCap2.set(CV_CAP_PROP_FRAME_HEIGHT, cameraSize.height);

    Size camera1Size = Size(m_videoCap1.get(CV_CAP_PROP_FRAME_WIDTH), m_videoCap1.get(CV_CAP_PROP_FRAME_HEIGHT));
    Size camera2Size = Size(m_videoCap2.get(CV_CAP_PROP_FRAME_WIDTH), m_videoCap2.get(CV_CAP_PROP_FRAME_HEIGHT));

    if(!m_videoCap1.isOpened() || !m_videoCap2.isOpened()){
        std::cerr<<"Could not open cameras"<<std::endl;
        return false;
    }

    if(camera1Size != cameraSize || camera2Size != cameraSize){
        std::cerr<<"Could not open cameras. Camera previews do not have the requested size!!"<<std::endl;
    }

//    std::cerr<<"Camera 1 resolution: "<<camera1Size.width<<" "<<camera1Size.height<<std::endl;
//    std::cerr<<"Camera 2 resolution: "<<camera2Size.width<<" "<<camera2Size.height<<std::endl;
    m_cameraSize = cameraSize;

    if(!showPreviews) return true;

    Mat imageCam1, imageCam2;
    Mat composedPreview;

    for(;;){
        // get new frames from camera
        m_videoCap1 >> imageCam1;
        m_videoCap2 >> imageCam2;

        composedPreview = Mat(imageCam1.rows, 2*imageCam1.cols, imageCam1.type());

        Mat roiImgLeft = composedPreview(Rect(0, 0, imageCam1.cols, imageCam1.rows));
        Mat roiIMgRight = composedPreview(Rect(imageCam1.cols, 0, imageCam2.cols, imageCam2.rows));

        imageCam1.copyTo(roiImgLeft);
        imageCam2.copyTo(roiIMgRight);

        namedWindow("cameraPreviews", WINDOW_NORMAL);
        imshow("cameraPreviews", composedPreview);

        if(waitKey(30) >= 0) break;

    }

    return true;
}

bool CVideoCapture::captureStereoImage(Mat &leftImage, Mat &rightImage, bool saveImage, std::string leftImageName, std::string rightImageName)
{

    bool resLeft = true, resRight = true;
    m_camera1Image.release();
    m_camera2Image.release();
    if(!m_videoCap1.isOpened() || !m_videoCap2.isOpened()){
        std::cerr<<"Cameras are not opened"<<std::endl;
        return false;
    }


    while (!(m_videoCap1.grab() && m_videoCap2.grab())){
        std::cerr<<"could not capture frame"<<std::endl;
    }

    m_videoCap1.retrieve(leftImage);
    m_videoCap2.retrieve(rightImage);

    m_camera1Image = leftImage.clone();
    m_camera2Image = rightImage.clone();

    if(saveImage){
//        std::cerr<<"saving images :"<<leftImageName<<" "<<rightImageName<<std::endl;
        resLeft = imwrite(leftImageName, leftImage);
        resRight = imwrite(rightImageName, rightImage);
    }

//    namedWindow("leftImage", WINDOW_NORMAL);
//    namedWindow("rightImage", WINDOW_NORMAL);
//    imshow("leftImage", leftImage);
//    imshow("rightImage", rightImage);

    return resLeft & resRight;
}

bool CVideoCapture::captureStereoImage(std::string leftImageName, std::string rightImageName)
{

    m_camera1Image.release();
    m_camera2Image.release();
    if(!m_videoCap1.isOpened() || !m_videoCap2.isOpened()){
        std::cerr<<"Cameras are not opened"<<std::endl;
        return false;
    }

    Mat leftImage, rightImage;

    while (!(m_videoCap1.grab() && m_videoCap2.grab())){
        std::cerr<<"could not capture frame"<<std::endl;
    }

    m_videoCap1.retrieve(leftImage);
    m_videoCap2.retrieve(rightImage);

    bool resLeft = imwrite(leftImageName, leftImage);
    bool resRight = imwrite(rightImageName, rightImage);

    m_camera1Image = leftImage.clone();
    m_camera2Image = rightImage.clone();

    namedWindow("leftImage", WINDOW_NORMAL);
    namedWindow("rightImage", WINDOW_NORMAL);
    if(resLeft && resRight){
        imshow("leftImage", leftImage);
        imshow("rightImage", rightImage);
    }

    return (resLeft & resRight);
}

bool CVideoCapture::captureImage(std::string imageName)
{
    m_camera1Image.release();
    if(!m_videoCap1.isOpened()){
        std::cerr<<"Cameras is not opened"<<std::endl;
        return false;
    }

    Mat image;

    bool grabbedFrame = m_videoCap1.grab();

    if(!grabbedFrame){
        std::cerr<<"Could not grab camera frame"<<std::endl;
        return false;
    }

    m_videoCap1.retrieve(image);
    m_camera1Image = image.clone();
    bool saved = imwrite(imageName, image);

    namedWindow("leftImage", WINDOW_NORMAL);
    if(saved){
        imshow("leftImage", image);
    }

    return saved;
}

void CVideoCapture::closeCameras()
{
    if(m_videoCap1.isOpened())
        m_videoCap1.release();
    if(m_videoCap2.isOpened())
        m_videoCap2.release();
}

void CVideoCapture::stop()
{
    m_captureFrames = false;
}

bool CVideoCapture::areCamerasOpen()
{
    return (m_videoCap1.isOpened() & m_videoCap2.isOpened());
}

void CVideoCapture::run()
{

    m_captureFrames = true;
    if(!m_isStereoCamera){

        cv::Mat camFrame, image;
        if(!m_videoCap1.isOpened()){
            std::cerr<<"ERROR!!! Camera not opened!"<<std::endl;
            return;
        }
        while(m_captureFrames){
            m_videoCap1 >> camFrame;
            if(camFrame.empty()){
                std::cerr<<"empty camera frame "<<std::endl;
                continue;
            }
            image = camFrame.clone();
            emit newCameraFrame(image);
        }
    }
    else{

        Mat leftFrame, rightFrame;
        Mat leftImage, rightImage;

        if(!m_videoCap1.isOpened() || !m_videoCap2.isOpened()){
            std::cerr<<"ERROR!!! Cameras are not opened!"<<std::endl;
            return;
        }

#ifdef SHOW_EXECUTION_TIME
        double cam1Time = 0;
        double cam2Time = 0;
#endif
        while(m_captureFrames){

            if(!m_videoCap1.grab())
                return;
            else{
#ifdef SHOW_EXECUTION_TIME
                cam1Time = clock();
                std::cerr<<"Grabbed frame from camera 1 "<<cam1Time<<std::endl;
#endif
            }

            if(!m_videoCap2.grab())
                return;
            else{
#ifdef SHOW_EXECUTION_TIME
                cam2Time = clock();
                std::cerr<<"Grabbed frame from camer 2"<<cam2Time<<std::endl;
#endif
            }


#ifdef SHOW_EXECUTION_TIME
            std::cerr<<"Frames grab difference: "<<(double)(cam2Time - cam1Time)/CLOCKS_PER_SEC;
#endif
            m_videoCap1.retrieve(leftFrame);
            m_videoCap2.retrieve(rightFrame);


            if(leftFrame.empty() || rightFrame.empty()){
                std::cerr<<"empty camera frame "<<std::endl;
                continue;
            }

            leftImage = leftFrame.clone();
            rightImage = rightFrame.clone();

            emit newStereoCameraFrame(leftFrame, rightFrame);

        }
    }


}

