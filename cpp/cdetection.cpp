#include "cdetection.h"
#include "cstereodetection.h"
#include "cconversionutils.h"
#include <QDebug>

#define HAARCASCADES_PATH "haarcascades/"
#define FACE_HAARCASCADES "haarcascades/haarcascade_frontalface_alt.xml"

const Size CDetection::FACE_DETECTION_IMG_SIZE = Size(320, 240);

CDetection::CDetection()
{
    m_detection = new CStereoDetection();
    m_detectionType = FACE_DETECTION;
    m_faceCascade = CascadeClassifier(FACE_HAARCASCADES);
    if(m_faceCascade.empty())
        qDebug()<<"Error while loading face cascade";

    bool loaded = m_detection->initDetection(HAARCASCADES_PATH);
    qDebug()<<"Eye detection initialized "<<loaded;
}

void CDetection::detect(Mat leftFrame, Mat rightFrame, detectionType type, bool rotateImage)
{
    if(leftFrame.empty() || rightFrame.empty()) return;

    if(isRunning()){
        qDebug()<<"Thread "<<currentThreadId()<<" is processing another frame";
        return;
    }

    m_leftImage = leftFrame;
    m_rightImage = rightFrame;
    m_detectionType = type;
    m_rotateImage = rotateImage;
    start();

}

void CDetection::run()
{

    Rect faceLeftImg = Rect(), faceRightImg = Rect();
    Rect leftEyeRectLeftImg = Rect(), rightEyeRectLeftImg = Rect();
    Rect leftEyeRectRightImg = Rect(), rightEyeRectRightImg = Rect();
    cv::Point2d leftEyeLeftImg = Point2d(), rightEyeLeftImg = Point2d();
    cv::Point2d leftEyeRightImg = Point2d(), rightEyeRightImg = Point2d();

    if(m_rotateImage){

        bool leftImgOk = rotateImage90DegCC(m_leftImage);
        bool rightImgOk = rotateImage90DegCC(m_rightImage);

        if(!leftImgOk || !rightImgOk){
            emit detectionFinised(false, CConversionUtils::rectToQRectF(faceLeftImg), CConversionUtils::pointToQPointF(leftEyeLeftImg), CConversionUtils::pointToQPointF(rightEyeLeftImg),
                                  CConversionUtils::rectToQRectF(leftEyeRectLeftImg), CConversionUtils::rectToQRectF(rightEyeRectLeftImg),
                                  CConversionUtils::rectToQRectF(faceRightImg), CConversionUtils::pointToQPointF(leftEyeRightImg), CConversionUtils::pointToQPointF(rightEyeRightImg),
                                  CConversionUtils::rectToQRectF(leftEyeRectRightImg), CConversionUtils::rectToQRectF(rightEyeRectRightImg));
            return;
        }
    }

    bool loaded = m_detection->loadImages(m_leftImage, m_rightImage);
    if(!loaded){
//        std::cerr<<"Images could not be loaded"<<std::endl;
        return;
    }

    bool detected = m_detection->detectFaces();
    if(detected){
        faceLeftImg = m_detection->getFaceRectangle(CStereoDetection::LEFT_IMAGE);
        faceRightImg = m_detection->getFaceRectangle(CStereoDetection::RIGHT_IMAGE);

        if(m_detectionType == EYES_DETECTION){
            detected = detected & m_detection->detectEyes();

            if(detected){
                leftEyeLeftImg = m_detection->getEyesCenter(CStereoDetection::EYE_POINT_LEFT, CStereoDetection::LEFT_IMAGE);
                rightEyeLeftImg = m_detection->getEyesCenter(CStereoDetection::EYE_POINT_RIGHT, CStereoDetection::LEFT_IMAGE);

                leftEyeRightImg = m_detection->getEyesCenter(CStereoDetection::EYE_POINT_LEFT, CStereoDetection::RIGHT_IMAGE);
                rightEyeRightImg = m_detection->getEyesCenter(CStereoDetection::EYE_POINT_RIGHT, CStereoDetection::RIGHT_IMAGE);

                leftEyeRectLeftImg = m_detection->getEyesRectangle(CStereoDetection::EYE_POINT_LEFT, CStereoDetection::LEFT_IMAGE);
                rightEyeRectLeftImg = m_detection->getEyesRectangle(CStereoDetection::EYE_POINT_RIGHT, CStereoDetection::LEFT_IMAGE);

                leftEyeRectRightImg = m_detection->getEyesRectangle(CStereoDetection::EYE_POINT_LEFT, CStereoDetection::RIGHT_IMAGE);
                rightEyeRectRightImg = m_detection->getEyesRectangle(CStereoDetection::EYE_POINT_RIGHT, CStereoDetection::RIGHT_IMAGE);

            }
        }

        if(m_detectionType == PUPILS_DETECTION){
            detected = detected & m_detection->detectCornealReflection();

            if(detected){
                leftEyeLeftImg = m_detection->getCornealReflection(CStereoDetection::EYE_POINT_LEFT, CStereoDetection::LEFT_IMAGE);
                rightEyeLeftImg = m_detection->getCornealReflection(CStereoDetection::EYE_POINT_RIGHT, CStereoDetection::LEFT_IMAGE);

                leftEyeRightImg = m_detection->getCornealReflection(CStereoDetection::EYE_POINT_LEFT, CStereoDetection::RIGHT_IMAGE);
                rightEyeRightImg = m_detection->getCornealReflection(CStereoDetection::EYE_POINT_RIGHT, CStereoDetection::RIGHT_IMAGE);

                leftEyeRectLeftImg = m_detection->getEyesRectangle(CStereoDetection::EYE_POINT_LEFT, CStereoDetection::LEFT_IMAGE);
                rightEyeRectLeftImg = m_detection->getEyesRectangle(CStereoDetection::EYE_POINT_RIGHT, CStereoDetection::LEFT_IMAGE);

                leftEyeRectRightImg = m_detection->getEyesRectangle(CStereoDetection::EYE_POINT_LEFT, CStereoDetection::RIGHT_IMAGE);
                rightEyeRectRightImg = m_detection->getEyesRectangle(CStereoDetection::EYE_POINT_RIGHT, CStereoDetection::RIGHT_IMAGE);
            }
        }

    }

    emit detectionFinised(detected, CConversionUtils::rectToQRectF(faceLeftImg), CConversionUtils::pointToQPointF(leftEyeLeftImg), CConversionUtils::pointToQPointF(rightEyeLeftImg),
                          CConversionUtils::rectToQRectF(leftEyeRectLeftImg), CConversionUtils::rectToQRectF(rightEyeRectLeftImg),
                          CConversionUtils::rectToQRectF(faceRightImg), CConversionUtils::pointToQPointF(leftEyeRightImg), CConversionUtils::pointToQPointF(rightEyeRightImg),
                          CConversionUtils::rectToQRectF(leftEyeRectRightImg), CConversionUtils::rectToQRectF(rightEyeRectRightImg));

}

bool CDetection::rotateImage90DegCC(Mat &image)
{
    if(image.empty()) return false;
    cv::transpose(image, image);
    cv::flip(image, image, 0);

    return true;
}


