#ifndef CDETECTIONTHREAD_H
#define CDETECTIONTHREAD_H

#include <QObject>
#include <QThread>
#include <QRectF>

#include "types.h"
#include "opencvheaders.h"

class CStereoDetection;
class CDetection : public QThread
{
    Q_OBJECT

public:

    explicit CDetection();

signals:
    void detectionFinised(bool detected, QRectF faceLeftImg, QPointF leftEyeLeftImg, QPointF rightEyeLeftImg, QRectF leftEyeRectLeftImg, QRectF rightEyeRectLeftImg, QRectF faceRightImg, QPointF leftEyeRightImg, QPointF rightEyeRightImg, QRectF leftEyeRectRightImg, QRectF rightEyeRectRightImg);

public slots:
    void detect(Mat leftFrame, Mat rightFrame, detectionType type, bool rotatedImage = false);

protected:
    void run();

private:


    CStereoDetection *m_detection;
    Mat m_leftImage, m_rightImage;
    CascadeClassifier m_faceCascade;
    detectionType m_detectionType;

    bool m_rotateImage;
    bool rotateImage90DegCC(Mat &image);

    static const Size FACE_DETECTION_IMG_SIZE;

};

#endif // CDETECTIONTHREAD_H
