#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <QCoreApplication>
#include <QtQml>
#include <QQmlEngine>
#include "pdscannerinitializer.h"
#include "cvideocontroller.h"
#include <QApplication>
#include <QQmlEngine>
#include "eyesimageprovider.h"
#include "systemutils.h"

PDScannerInitializer::PDScannerInitializer()
{
    m_videoController = NULL;
    m_eyesImageProvider = NULL;
    m_sysUtils = NULL;
}

PDScannerInitializer::~PDScannerInitializer()
{
    freeAplicationResources();
}

void PDScannerInitializer::initApplication(QQuickView &viewer)
{
    if(!m_videoController)
        m_videoController = new CVideoController();

    viewer.rootContext()->setContextProperty("videoController", m_videoController);

    m_eyesImageProvider = new EyesImageProvider(QQuickImageProvider::Image);
    viewer.engine()->addImageProvider("eyesprovider", m_eyesImageProvider);
    m_videoController->setEyesImageProvider(m_eyesImageProvider);

    m_sysUtils = new SystemUtils();
    viewer.rootContext()->setContextProperty("sysUtils", m_sysUtils);

    QString appDir = "file://" + QApplication::applicationDirPath() + "/";
    viewer.rootContext()->setContextProperty("appDir", appDir);
}

void PDScannerInitializer::freeAplicationResources()
{
    delete m_videoController;
    m_videoController = NULL;
    delete m_sysUtils;
    m_sysUtils = NULL;
}
