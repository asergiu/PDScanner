#include "eyesimageprovider.h"
#include <QDebug>

EyesImageProvider::EyesImageProvider(ImageType type) :
    QQuickImageProvider(type)
{
}

QImage EyesImageProvider::requestImage(const QString &id, QSize *, const QSize &requestedSize)
{
    QImage img(requestedSize.width(), requestedSize.height(), QImage::Format_RGB888);
    if (id == "lefteye1") {
        if (m_leftEye1.isNull())
            img.fill(Qt::red);
        else
            return m_leftEye1;
    }
    else if (id == "righteye1") {
        if (m_rightEye1.isNull())
            img.fill(Qt::green);
        else
            return m_rightEye1;
    }
    else if (id == "lefteye2") {
        if (m_leftEye2.isNull())
            img.fill(Qt::blue);
        else
            return m_leftEye2;
    }
    else if (id == "righteye2") {
        if (m_rightEye2.isNull())
            img.fill(Qt::gray);
        else
            return m_rightEye2;
    }
    else {
//        qDebug() << "EyesImageProvider::requestImage(): invalid image id: " << id;
        return QImage();
    }
    return img;
}

QPixmap EyesImageProvider::requestPixmap(const QString &, QSize *, const QSize &)
{
    return QPixmap();
}

void EyesImageProvider::setLeftEyeImage1(QImage img)
{
    m_leftEye1 = img;
}

void EyesImageProvider::setRightEyeImage1(QImage img)
{
    m_rightEye1 = img;
}

void EyesImageProvider::setLeftEyeImage2(QImage img)
{
    m_leftEye2 = img;
}

void EyesImageProvider::setRightEyeImage2(QImage img)
{
    m_rightEye2 = img;
}
