#ifndef PDSCANNERINITIALIZER_H
#define PDSCANNERINITIALIZER_H

#include <QtQuick/QQuickView>

class CVideoController;
class EyesImageProvider;
class SystemUtils;

class PDScannerInitializer
{
public:
    PDScannerInitializer();
    ~PDScannerInitializer();
    void initApplication(QQuickView &viewer);
    void freeAplicationResources();

private:
    CVideoController *m_videoController;
    EyesImageProvider *m_eyesImageProvider;
    SystemUtils *m_sysUtils;
};

#endif // PDSCANNERINITIALIZER_H
