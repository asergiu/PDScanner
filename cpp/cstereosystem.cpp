#include "cstereosystem.h"

#include <vector>
using namespace std;

const QString CStereoSystem::IMAGES_XML = QString("stereo_calib.xml");

CStereoSystem::CStereoSystem(QObject *parent) :
    QObject(parent)
{
    clearCalibrationData();
}

void CStereoSystem::clearCalibrationData()
{

    m_imageSize = Size();
    m_stereoParamsFile = QString();

    m_cameraMatrix[0].release();
    m_cameraMatrix[0] = Mat();

    m_cameraMatrix[1].release();
    m_cameraMatrix[1] = Mat();

    m_distCoeffs[0].release(); m_distCoeffs[0] = Mat();
    m_distCoeffs[1].release(); m_distCoeffs[1] = Mat();

    m_T.release(); m_T = Mat();
    m_R.release(); m_R = Mat();
    m_R1.release(); m_R1 = Mat();
    m_R2.release(); m_R2 = Mat();

}

bool CStereoSystem::loadCalibrationImages( const QString imagesPath, QStringList &l)
{

    l.clear();
    FileStorage fs(IMAGES_XML.toUtf8().data(), FileStorage::READ);
    if( !fs.isOpened() )
        return false;
    FileNode n = fs.getFirstTopLevelNode();
    if( n.type() != FileNode::SEQ )
        return false;
    FileNodeIterator it = n.begin(), it_end = n.end();
    //    int i = 0;
    for( ; it != it_end; ++it ){
        //        i++;
        //        if(i > 13*2) break;
        QString filePath = imagesPath;
        filePath = filePath.append(QString(((string)*it).c_str()));
        l.push_back(filePath);
    }
    return true;

}


float CStereoSystem::calibrateAndRectify(const QString imagesPath, QSize qBoardSize, const float squareSize, bool displayCorners, bool showRectified, bool transpose)
{

    Size boardSize = cv::Size(qBoardSize.width(), qBoardSize.height());
    QStringList imageslist = QStringList();
    loadCalibrationImages(imagesPath, imageslist);

    if( imageslist.size() % 2 != 0 )
    {
        qDebug() << "Error: the image list contains odd (non-even) number of elements\n";
        return -1;
    }

    const int maxScale = 2;

    vector<vector<Point2f> > imagePoints[2];
    vector<vector<Point3f> > objectPoints;

    int i, j, k, nimages = (int)imageslist.size()/2;

    imagePoints[0].resize(nimages);
    imagePoints[1].resize(nimages);
    vector<string> goodImageList;

    for( i = j = 0; i < nimages; i++ )
    {
        for( k = 0; k < 2; k++ )
        {
            std::string filename = imageslist[i*2+k].toUtf8().data();
            Mat img = imread(filename, 0);

            if(img.empty())
                break;

//            std::cerr<<"image name is "<<filename<<std::endl;

            if(transpose){
                cv::transpose(img, img);
                cv::flip(img, img, 0);
            }

            if( m_imageSize == Size() )
                m_imageSize = img.size();
            else if( img.size() != m_imageSize )
            {
                qDebug() << "The image " << imageslist[i*2+k] << " has the size different from the first image size. Skipping the pair\n";
                break;
            }
            bool found = false;
            vector<Point2f>& corners = imagePoints[k][j];
            for( int scale = 1; scale <= maxScale; scale++ )
            {
                Mat timg;
                if( scale == 1 )
                    timg = img;
                else
                    resize(img, timg, Size(), scale, scale);

                found = findChessboardCorners(timg, boardSize, corners,
                                              CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE);
                if( found )
                {
                    if( scale > 1 )
                    {
                        Mat cornersMat(corners);
                        cornersMat *= 1./scale;
                    }
                    break;
                }else
                    qDebug()<<"could not found corners in input image "<<imageslist[i*2+k];
            }
            if( displayCorners )
            {
                Mat cimg, cimg1;
                cvtColor(img, cimg, CV_GRAY2BGR);
                drawChessboardCorners(cimg, boardSize, corners, found);
                double sf = 640./MAX(img.rows, img.cols);
                resize(cimg, cimg1, Size(), sf, sf);
                namedWindow("corners", WINDOW_NORMAL);
                imshow("corners", cimg1);
                char c = (char)waitKey(0);
                if( c == 27 || c == 'q' || c == 'Q' ) //Allow ESC to quit
                    exit(-1);
            }
            else
                putchar('.');
            if( !found )
                break;
            cornerSubPix(img, corners, Size(11,11), Size(-1,-1),
                         TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,
                                      30, 0.01));
        }
        if( k == 2 )
        {
            goodImageList.push_back(imageslist[i*2].toUtf8().data());
            goodImageList.push_back(imageslist[i*2+1].toUtf8().data());
            j++;
        }
    }
    qDebug() << j << " pairs have been successfully detected.\n";
    nimages = j;
    if( nimages < 2 )
    {
        qDebug() << "Error: too little pairs to run the calibration\n";
        return -1;
    }

    imagePoints[0].resize(nimages);
    imagePoints[1].resize(nimages);
    objectPoints.resize(nimages);

    for( i = 0; i < nimages; i++ )
    {
        for( j = 0; j < boardSize.height; j++ )
            for( k = 0; k < boardSize.width; k++ )
                objectPoints[i].push_back(Point3f(j*squareSize, k*squareSize, 0));
    }

    qDebug() << "Running stereo calibration ...\n";

    m_cameraMatrix[0] = Mat::eye(3, 3, CV_64F);
    m_cameraMatrix[1] = Mat::eye(3, 3, CV_64F);

    setIdentity(m_cameraMatrix[0]);
    setIdentity(m_cameraMatrix[1]);
    m_distCoeffs[0].setTo(0);
    m_distCoeffs[1].setTo(0);

    double rms = cv::stereoCalibrate(objectPoints, imagePoints[0], imagePoints[1],
            m_cameraMatrix[0], m_distCoeffs[0],
            m_cameraMatrix[1], m_distCoeffs[1],
            m_imageSize, m_R, m_T, m_E, m_F,
            TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 100, 1e-5),
            //             CV_CALIB_FIX_ASPECT_RATIO+CV_CALIB_ZERO_TANGENT_DIST + CV_CALIB_SAME_FOCAL_LENGTH
            CV_CALIB_FIX_ASPECT_RATIO +
            CV_CALIB_ZERO_TANGENT_DIST +
            CV_CALIB_FIX_PRINCIPAL_POINT +
            //            CV_CALIB_RATIONAL_MODEL +
            CV_CALIB_FIX_K3 + CV_CALIB_FIX_K4 + CV_CALIB_FIX_K5);


    qDebug() << "done with RMS error=" << rms;

    // CALIBRATION QUALITY CHECK
    // because the output fundamental matrix implicitly
    // includes all the output information,
    // we can check the quality of calibration using the
    // epipolar geometry constraint: m2^t*F*m1=0
    double err = 0;
    int npoints = 0;
    vector<Vec3f> lines[2];
    for( i = 0; i < nimages; i++ )
    {
        int npt = (int)imagePoints[0][i].size();
        Mat imgpt[2];
        for( k = 0; k < 2; k++ )
        {
            imgpt[k] = Mat(imagePoints[k][i]);
            undistortPoints(imgpt[k], imgpt[k], m_cameraMatrix[k], m_distCoeffs[k], Mat(), m_cameraMatrix[k]);
            computeCorrespondEpilines(imgpt[k], k+1, m_F, lines[k]);
        }
        for( j = 0; j < npt; j++ )
        {
            double errij = fabs(imagePoints[0][i][j].x*lines[1][j][0] +
                    imagePoints[0][i][j].y*lines[1][j][1] + lines[1][j][2]) +
                    fabs(imagePoints[1][i][j].x*lines[0][j][0] +
                    imagePoints[1][i][j].y*lines[0][j][1] + lines[0][j][2]);
            err += errij;
        }
        npoints += npt;
    }
    cout << "average reprojection err = " <<  err/npoints << endl;


    saveIntrinsicsParams(m_stereoParamsFile, CV_STORAGE_WRITE);


    /// ************************************ stereo rectify **********************************************************
    ///  Operation flags that may be zero or CV_CALIB_ZERO_DISPARITY . If the flag is set, the function makes the principal
    ///  points of each camera have the same pixel coordinates in the rectified views. And if the flag is not set, the function
    ///  may still shift the images in the horizontal or vertical direction (depending on the orientation of epipolar lines) to maximize the useful image area.
    ///  IMPORTANT!!!! if CV_CALIB_ZERO_DISPARITY is not set the disparity, then x_left and x_right are not counted from the image center, but rather from
    ///  the respective principal points c_x_left and c_x_right, which could differ for the left and the right image. Therefore, the disparity should be adjusted
    ///  so that d = x_left - x_right - (c_x_left - c_x_right).
    /// ***************************************************************************************************************

    stereoRectify(m_cameraMatrix[0], m_distCoeffs[0],
            m_cameraMatrix[1], m_distCoeffs[1],
            m_imageSize, m_R, m_T, m_R1, m_R2, m_P1, m_P2, m_Q,
            /*CALIB_ZERO_DISPARITY*/0, 1, m_imageSize, &m_validRoi[0], &m_validRoi[1]);

    // OpenCV can handle left-right
    // or up-down camera arrangements
    m_isVerticalStereo = fabs(m_P2.at<double>(1, 3)) > fabs(m_P2.at<double>(0, 3));
    qDebug()<<"is vertical stereo "<<m_isVerticalStereo;

    //Precompute maps for cv::remap()
    initUndistortRectifyMap(m_cameraMatrix[0], m_distCoeffs[0], m_R1, m_P1, m_imageSize, CV_16SC2, m_rmap[0][0], m_rmap[0][1]);
    initUndistortRectifyMap(m_cameraMatrix[1], m_distCoeffs[1], m_R2, m_P2, m_imageSize, CV_16SC2, m_rmap[1][0], m_rmap[1][1]);

    qDebug()<<"File is "<<m_stereoParamsFile;
    saveExtrinsicParameters(m_stereoParamsFile, CV_STORAGE_APPEND);


    // COMPUTE AND DISPLAY RECTIFICATION
    if( !showRectified )
        return rms;

    Mat canvas;
    double sf = 1;
    int w, h;
    if( !m_isVerticalStereo )
    {
        w = m_imageSize.width;
        h = m_imageSize.height;
        canvas.create(h, w*2, CV_8UC3);
    }
    else
    {
        w = m_imageSize.width;
        h = m_imageSize.height;
        canvas.create(h*2, w, CV_8UC3);
    }

    for( i = 0; i < nimages; i++ )
    {
        for( k = 0; k < 2; k++ )
        {
            Mat img = imread(goodImageList[i*2+k], 0), rimg, cimg;

            remap(img, rimg, m_rmap[k][0], m_rmap[k][1], CV_INTER_LINEAR);
            cvtColor(rimg, cimg, CV_GRAY2BGR);
            Mat canvasPart = !m_isVerticalStereo ? canvas(Rect(w*k, 0, w, h)) : canvas(Rect(0, h*k, w, h));
            resize(cimg, canvasPart, canvasPart.size(), 0, 0, CV_INTER_AREA);


            Rect vroi(cvRound(m_validRoi[k].x*sf), cvRound(m_validRoi[k].y*sf),
                      cvRound(m_validRoi[k].width*sf), cvRound(m_validRoi[k].height*sf));
            rectangle(canvasPart, vroi, Scalar(0,0,255), 3, 8);
        }

        if( !m_isVerticalStereo )
            for( j = 0; j < canvas.rows; j += 16 )
                line(canvas, Point(0, j), Point(canvas.cols, j), Scalar(0, 255, 0), 1, 8);
        else
            for( j = 0; j < canvas.cols; j += 16 )
                line(canvas, Point(j, 0), Point(j, canvas.rows), Scalar(0, 255, 0), 1, 8);
        namedWindow("rectified", WINDOW_NORMAL);
        imshow("rectified", canvas);
        char c = (char)waitKey();
        if( c == 27 || c == 'q' || c == 'Q' )
            break;
    }
    return rms;
}


bool CStereoSystem::rectifyImages(QString leftImage, QString rightImage,  QString leftRectified , QString rightRectified, bool transpose)
{

    Mat leftImageMat = imread(leftImage.toUtf8().data(), 0);
    Mat rightImageMat = imread(rightImage.toUtf8().data(), 0);

    if(!rectifyImages(leftImageMat, rightImageMat, transpose))
        return false;

    imwrite(leftRectified.toUtf8().data(), m_leftRectifiedImage);
    imwrite(rightRectified.toUtf8().data(), m_rightRectifiedImage);

    return true;
}

bool CStereoSystem::rectifyImagesColor(QString leftImage, QString rightImage,  QString leftRectified , QString rightRectified, bool transpose)
{

    Mat leftImageMat = imread(leftImage.toUtf8().data(), 1);
    Mat rightImageMat = imread(rightImage.toUtf8().data(), 1);

    if(!rectifyImages(leftImageMat, rightImageMat, transpose))
        return false;

    imwrite(leftRectified.toUtf8().data(), m_leftRectifiedImage);
    imwrite(rightRectified.toUtf8().data(), m_rightRectifiedImage);

    return true;
}

bool CStereoSystem::saveIntrinsicsParams(QString intrinsicsFile, int mode){

    if(m_cameraMatrix[0].empty() || m_cameraMatrix[1].empty() || m_distCoeffs[0].empty() || m_distCoeffs[1].empty())
        return false;

    // save intrinsic parameters
    FileStorage fs(intrinsicsFile.toUtf8().data(), mode);
    if( fs.isOpened() )
    {
        fs << "M1" << m_cameraMatrix[0] << "D1" << m_distCoeffs[0] <<
              "M2" << m_cameraMatrix[1] << "D2" << m_distCoeffs[1] << "SZ" <<m_imageSize;
        fs.release();
        return true;
    }
    else
        qDebug() << "Error: can not save the intrinsic parameters\n";

    return false;
}

bool CStereoSystem::loadIntrinsicsParams(QString intrinsicsFile)
{

    FileStorage fs;
    fs.open(intrinsicsFile.toUtf8().data(), CV_STORAGE_READ);

    if(!fs.isOpened()) return false;

    fs["M1"] >> m_cameraMatrix[0];
    fs["M2"] >> m_cameraMatrix[1];
    fs["D1"] >> m_distCoeffs[0];
    fs["D2"] >> m_distCoeffs[1];

    std::vector<int> size;
    fs["SZ"] >> size;
    m_imageSize = cv::Size(size[0], size[1]);
    fs.release();

    return true;
}



bool CStereoSystem::saveExtrinsicParameters(QString extrinsicsFile,  int mode){

    FileStorage fs;
    fs.open(extrinsicsFile.toUtf8().data(), mode);
    if( fs.isOpened() )
    {
        fs << "R" << m_R << "T" << m_T << "R1" << m_R1 << "R2" << m_R2 << "P1" << m_P1 << "P2" << m_P2 << "Q" << m_Q;

        fs.release();
        return true;
    }
    else
        qDebug() << "Error: can not save the intrinsic parameters\n";

    return false;
}

bool CStereoSystem::loadExtrinsicsParams(QString extrinsicsFile)
{

    FileStorage fs;
    fs.open(extrinsicsFile.toUtf8().data(), CV_STORAGE_READ);
    if(!fs.isOpened()) return false;

    fs["R"] >> m_R;
    fs["T"] >> m_T;
    fs["R1"] >> m_R1;
    fs["R2"] >> m_R2;
    fs["P1"] >> m_P1;
    fs["P2"] >> m_P2;
    fs["Q"] >> m_Q;

    fs.release();

    m_isVerticalStereo = fabs(m_P2.at<double>(1, 3)) > fabs(m_P2.at<double>(0, 3));

    initUndistortRectifyMap(m_cameraMatrix[0], m_distCoeffs[0], m_R1, m_P1, m_imageSize, CV_16SC2, m_rmap[0][0], m_rmap[0][1]);
    initUndistortRectifyMap(m_cameraMatrix[1], m_distCoeffs[1], m_R2, m_P2, m_imageSize, CV_16SC2, m_rmap[1][0], m_rmap[1][1]);

    return true;
}

bool CStereoSystem::saveStereoSystemParams(QString file)
{
    saveIntrinsicsParams(file, CV_STORAGE_WRITE);
    saveExtrinsicParameters(file, CV_STORAGE_APPEND);
    return true;
}

bool CStereoSystem::loadStereoSystemParams(QString file)
{
    bool intrLoaded = loadIntrinsicsParams(file);
    bool extrLoaded = loadExtrinsicsParams(file);
    return (intrLoaded & extrLoaded);
}

///
/// \brief StereoCalibration::compute3DPosition: compute 3D position of a point, given the point`s coordinates in the left and right images
/// \param imagePoint1 - point position in image from left camera
/// \param imagePoint2 - point position in image from right camera
/// \return the 3D position of the point
///
QVector3D CStereoSystem::compute3DPosition(QPointF imagePoint1, QPointF imagePoint2)
{

    //    cv::Point2d imagePoint1CV, imagePoint2CV;
    //    cv::Point3d point3DCV = cv::Point3d();

    //    imagePoint1CV = Point2d(imagePoint1.x(), imagePoint1.y());
    //    imagePoint2CV = Point2d(imagePoint2.x(), imagePoint2.y());

    //    point3DCV = compute3DPosition(imagePoint1CV, imagePoint2CV);

    //    return QVector3D(point3DCV.x, point3DCV.y, point3DCV.z);

    /// disparity
    double d = 0;
    /// principal point difference - if the flag CALIB_ZERO_DISPARITY is not set, then the image coordinates of the point must be conted
    /// from the principal points (which may differ for the left and right camera). Therefore, we must compute the principal point difference;
    double diff = 0;


    /// compute disparity: d = x_left - x_right - (c_x_left - c_x_right) (for horizontal stereo systems)
    /// or  d = y_left - y_right - (c_y_left - c_y_right) (for horizontal stereo systems)
    if(!m_isVerticalStereo){
        diff = m_cameraMatrix[0].at<double>(0, 2) - m_cameraMatrix[1].at<double>(0, 2);
        d = imagePoint1.x() - imagePoint2.x() - diff;
    }
    else{
        diff = m_cameraMatrix[0].at<double>(1, 2) - m_cameraMatrix[1].at<double>(1, 2);;
        d = imagePoint1.y() - imagePoint2.y() - diff;
    }

    cv::Point3d point3d;
    double W = 0;

    /// compute the 3D position of the point given the reprojection matrix
    /// [x y d 1]t * Q = [X Y Z W]t
    point3d.x = imagePoint1.x() * m_Q.at<double>(0, 0) + m_Q.at<double>(0, 3);
    point3d.y = imagePoint1.y() * m_Q.at<double>(1, 1) + m_Q.at<double>(1, 3);
    point3d.z = m_Q.at<double>(2, 3);
    W = d * m_Q.at<double>(3, 2) + m_Q.at<double>(3, 3);

    /// the coordinates of the point in 3D are X3D = X/W, Y3D = Y/W, Z3D = Z/W
    point3d.x = point3d.x / W;
    point3d.y = point3d.y / W;
    point3d.z = point3d.z / W;

    return QVector3D(point3d.x, point3d.y, point3d.z);
}

Point3d CStereoSystem::compute3DPositionUndistoredPoints(Point2d pointLeftImage, Point2d pointRightImage)
{
    vector<Point2d> inputPoints = vector<Point2d>();
    vector<Point2d> undistortedPoints = vector<Point2d>();

    Point2d pointLeftImageUndistored = Point2d(), pointRightImageUndistorted = Point2d();

    /// 1. undistort points
    inputPoints.push_back(pointLeftImage);
    undistortPoints(inputPoints, undistortedPoints, m_cameraMatrix[0], m_distCoeffs[0], m_R1, m_P1);
    if(undistortedPoints.size() > 0)
        pointLeftImageUndistored = undistortedPoints.at(0);

    inputPoints.clear(); inputPoints.push_back(pointRightImage);
    undistortedPoints.clear();
    undistortPoints(inputPoints, undistortedPoints, m_cameraMatrix[1], m_distCoeffs[1], m_R2, m_P2);
    if(undistortedPoints.size() > 0)
        pointRightImageUndistorted = undistortedPoints.at(0);


    /// 2. computed 3d position based on reprojection matrix Q
    return compute3DPosition(pointLeftImageUndistored, pointRightImageUndistorted);
}

Point3d CStereoSystem::compute3DPosition(Point2d pointLeftImage, Point2d pointRightImage)
{
    /// disparity
    double d = 0;
    /// principal point difference - if the flag CALIB_ZERO_DISPARITY is not set, then the image coordinates of the point must be counted
    /// from the principal points (which may differ for the left and right camera). Therefore, we must compute the principal point difference;
    double diff = 0;


    /// compute disparity: d = x_left - x_right - (c_x_left - c_x_right) (for horizontal stereo systems)
    /// or  d = y_left - y_right - (c_y_left - c_y_right) (for horizontal stereo systems)
    if(!m_isVerticalStereo){
        diff = m_cameraMatrix[0].at<double>(0, 2) - m_cameraMatrix[1].at<double>(0, 2);
        d = pointLeftImage.x - pointRightImage.x - diff;
    }
    else{
        diff = m_cameraMatrix[0].at<double>(1, 2) - m_cameraMatrix[1].at<double>(1, 2);;
        d = pointLeftImage.y - pointRightImage.y - diff;
    }

    cv::Point3d point3d;
    double W = 0;

    /// compute the 3D position of the point given the reprojection matrix
    /// [x y d 1]t * Q = [X Y Z W]t
    point3d.x = pointLeftImage.x * m_Q.at<double>(0, 0) + m_Q.at<double>(0, 3);
    point3d.y = pointLeftImage.y * m_Q.at<double>(1, 1) + m_Q.at<double>(1, 3);
    point3d.z = m_Q.at<double>(2, 3);
    W = d * m_Q.at<double>(3, 2) + m_Q.at<double>(3, 3);

    /// the coordinates of the point in 3D are X3D = X/W, Y3D = Y/W, Z3D = Z/W
    point3d.x = point3d.x / W;
    point3d.y = point3d.y / W;
    point3d.z = point3d.z / W;

    return point3d;
}

bool CStereoSystem::rectifyImages(Mat leftImageMat, Mat rightImageMat, bool transpose)
{
    if(leftImageMat.empty() || rightImageMat.empty()) return false;

    if(transpose){
        cv::transpose(leftImageMat, leftImageMat);
        cv::transpose(rightImageMat, rightImageMat);
        cv::flip(leftImageMat, leftImageMat, 0);
        cv::flip(rightImageMat, rightImageMat, 0);
    }

    m_leftRectifiedImage.release(); m_rightRectifiedImage.release();
    m_leftRectifiedImage = Mat(); m_rightRectifiedImage = Mat();

    initUndistortRectifyMap(m_cameraMatrix[0], m_distCoeffs[0], m_R1, m_P1, m_imageSize, CV_16SC2, m_rmap[0][0], m_rmap[0][1]);
    initUndistortRectifyMap(m_cameraMatrix[1], m_distCoeffs[1], m_R2, m_P2, m_imageSize, CV_16SC2, m_rmap[1][0], m_rmap[1][1]);

    remap(leftImageMat, m_leftRectifiedImage, m_rmap[0][0], m_rmap[0][1], CV_INTER_LINEAR);
    remap(rightImageMat, m_rightRectifiedImage,  m_rmap[1][0], m_rmap[1][1], CV_INTER_LINEAR);

    return true;
}

bool CStereoSystem::isVerticalStereo(){
    return m_isVerticalStereo;
}

void CStereoSystem::setStereoParamsFile(QString fileName){
    m_stereoParamsFile = fileName;
}

bool CStereoSystem::stereoSystemInitialized()
{
    if(m_cameraMatrix[0].empty() || m_cameraMatrix[1].empty() || m_R1.empty() || m_R2.empty() || m_Q.empty() || m_P1.empty() || m_P2.empty()
            || m_distCoeffs[0].empty() || m_distCoeffs[1].empty() || m_imageSize == Size())
        return false;
    return true;
}

Mat CStereoSystem::getRectififedImage(int imageIndex){
    if(imageIndex == 0)
        return m_leftRectifiedImage;

    return m_rightRectifiedImage;
}


bool CStereoSystem::setRoi(const Mat &image, cv::Rect roiRect, cv::Mat &roiMat){


    roiMat = Mat(roiRect.size(), CV_8UC3);
    roiMat.setTo(0);

    Rect inImageRect = roiRect;
    if(inImageRect.x < 0)
        inImageRect.x = 0;
    if(inImageRect.y < 0)
        inImageRect.y = 0;

    if(inImageRect.x + inImageRect.width > image.cols)
        inImageRect.width = image.cols - inImageRect.x;

    if(inImageRect.y + inImageRect.height > image.rows)
        inImageRect.height = image.rows - inImageRect.y;


    Mat originalImageRoi = Mat(image, inImageRect);


    copyMakeBorder( originalImageRoi, roiMat, inImageRect.y - roiRect.y, roiRect.height - inImageRect.height, inImageRect.x - roiRect.x, roiRect.width - inImageRect.width, BORDER_CONSTANT, 0 );


    //    namedWindow("originalImage", WINDOW_NORMAL);
    //    imshow("originalImage", image);

    //    namedWindow("origroi");
    //    imshow("origroi", originalImageRoi);

    //    namedWindow("roi", WINDOW_NORMAL);
    //    imshow("roi", roiMat);
    //    waitKey(0);

    return true;

}

bool CStereoSystem::enhanceColorImage(Mat inputImage, Mat &result)
{
    if(inputImage.empty() || inputImage.channels() != 3) return false;


    Mat convert = Mat(inputImage.size(), CV_8UC3);
    result = Mat(inputImage.size(), CV_8UC3);

    int units = 10;

    int heightc = convert.rows;
    int widthc = convert.cols;

    int stepc=convert.step;
    int channelsc=convert.channels();
    uchar *datac = (uchar *)convert.data;

    cvtColor(inputImage,convert,CV_BGR2HSV);

    int i, j;
    int temp = 0;
    for(i=0;i< (heightc);i++)
        for(j=0;j<(widthc);j++)
        {
            temp=datac[i*stepc+j*channelsc+1]+units;/*increase the saturaion component is the second arrray.*/

            if(temp>255)
                datac[i*stepc+j*channelsc+1]=255;
            else
                datac[i*stepc+j*channelsc+1]=temp;
        }

    cvtColor(convert, result, CV_HSV2BGR);

    namedWindow("original", WINDOW_NORMAL);
    namedWindow("Result", WINDOW_NORMAL);

    imshow("Result", result);
    imshow("original", inputImage);
    waitKey(0);

    destroyWindow("original");
    destroyWindow("Result");

    return true;
}






