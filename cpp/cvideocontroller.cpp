#include "cvideocontroller.h"
#include "cstereosystem.h"
#include "cvideocapture.h"
#include "cdetection.h"
#include "ctowerengine.h"
#include "cconversionutils.h"
#include <QDebug>
#include <QTest>
#include "eyesimageprovider.h"

const double CVideoController::TOTAL_TOWER_MOVE_DURATION_MSEC = 23000;
const double CVideoController::TOWER_MOVE_DURATION_MSEC = 2000;
const QString CVideoController::STEREO_SYSTEM_PARAMS = QString("stereoSystemParam.yml");
const std::string CVideoController::CAPTURE_WINDOW1 = "Camera Preview 1";
const std::string CVideoController::CAPTURE_WINDOW2 = "Camera Preview 2";
const std::string CVideoController::EYES_WINDOW1 = "Eyes Image 1";
const std::string CVideoController::EYES_WINDOW2 = "Eyes Image 2";

CVideoController::CVideoController(QObject *parent) :
    QObject(parent)
{
    qRegisterMetaType<Mat>("Mat");

    QVariant closestCamera = sysUtils.getConfigXMLTagValue("CameraNearLight");
    if (closestCamera.canConvert(QMetaType::Int)) {
        m_closestCameraToLight = closestCamera.toInt();
    } else {
        qDebug() << "CVideoController::CVideoController(): invalid CameraNearLight in config file: " << closestCamera;
        m_closestCameraToLight = 0;
    }

    m_saveImages = sysUtils.getConfigXMLTagValue("SaveImages").toBool();

    m_towerEngine = new CTowerEngine;
    m_performDetection = true;

    m_imageSize = QSize(1920, 1080);

    m_moveDuration = 0;
    m_moveDurationTimer.setInterval(TOWER_MOVE_DURATION_MSEC);
    m_moveDurationTimer.setSingleShot(false);
    connect(&m_moveDurationTimer, SIGNAL(timeout()), this, SLOT(onTowerStopMoving()));

    m_detectionThread = new CDetection;
    connect(m_detectionThread, SIGNAL(detectionFinised(bool,QRectF,QPointF,QPointF,QRectF,QRectF,QRectF,QPointF,QPointF,QRectF,QRectF)), this, SLOT(onDetectionResult(bool,QRectF,QPointF,QPointF,QRectF,QRectF,QRectF,QPointF,QPointF,QRectF,QRectF)));

    m_videoCapture = new CVideoCapture;
    m_videoCapture->openCameras(0, 1, Size(m_imageSize.width(), m_imageSize.height()), false);
    connect(m_videoCapture, SIGNAL(newStereoCameraFrame(Mat, Mat)), this, SLOT(processNewStereoCameraFrame(Mat, Mat)), Qt::UniqueConnection);

    m_stereoSystem = new CStereoSystem();
    if(!m_stereoSystem->loadStereoSystemParams(STEREO_SYSTEM_PARAMS)) qDebug()<<"Stereo system parameters not loaded";

    m_isDebugMode = false;
    m_eyesImageProvider = NULL;
}

CVideoController::~CVideoController(){

    if(m_videoCapture){
        m_videoCapture->stop();
        m_videoCapture->wait();
        m_videoCapture->quit();
        m_videoCapture->closeCameras();

        delete m_videoCapture;
        m_videoCapture = NULL;
    }

    if(m_towerEngine){
        delete m_towerEngine;
        m_towerEngine = NULL;
    }
}

bool CVideoController::startNewMeasurement()
{

    destroyAllWindows();

    if (m_isDebugMode) {
        namedWindow(CAPTURE_WINDOW1, WINDOW_NORMAL);
        namedWindow(CAPTURE_WINDOW2, WINDOW_NORMAL);
        moveWindow(CAPTURE_WINDOW1, 400, 450);
        moveWindow(CAPTURE_WINDOW2, 800, 450);
    }

    m_eyesDetectionFrames = 0;

    if(m_towerEngine->light())
        m_towerEngine->turnLights(false);

    m_stopDetection = false;
    if(!m_videoCapture->areCamerasOpen()){
        qDebug()<<"ERROR!!!! Could not get frames from cameras";
        return false;
    }

    if(!m_stereoSystem->stereoSystemInitialized()){
        qDebug()<<"ERROR!!! Stereo system parameters not loaded";
        return false;
    }

    m_moveDuration = 0;
    m_detectionType = FACE_DETECTION;
    m_performDetection = true;

    if(!m_videoCapture->isRunning())
        m_videoCapture->start();

    QTest::qSleep(500);

    if(m_detectionThread->isRunning()){
        m_detectionThread->wait();
        m_detectionThread->quit();
    }

    m_movingDirection = MOVING_DOWN;
    m_towerEngine->moveDown();

    m_moveDurationTimer.start();
    return true;

}

void CVideoController::stopMeasurement()
{
    m_moveDurationTimer.stop();
    m_towerEngine->stop();

    if(m_towerEngine->light()){
        m_towerEngine->turnLights(false);
    }
    moveCamerasToTop();

    m_performDetection = false;
    if(m_detectionThread->isRunning()){
        m_detectionThread->quit();
        m_detectionThread->wait();
    }
}

void CVideoController::moveCamerasToTop()
{
//    qDebug()<<"moving cameras up";

    if(m_towerEngine->light()){
        m_towerEngine->turnLights(false);
    }
    m_towerEngine->stop();
    m_towerEngine->moveUp();
    m_movingDirection = MOVING_UP;
    m_moveDuration = 0;

    m_moveDurationTimer.start(TOWER_MOVE_DURATION_MSEC);

}

void CVideoController::moveCamerasSingle(bool moveUp)
{

    if(m_moveDurationTimer.isActive()){
        m_moveDurationTimer.stop();
        m_towerEngine->stop();
    }

    if(m_detectionThread->isRunning()){
        m_detectionThread->wait();
        m_detectionThread->quit();
    }

    m_stopDetection = true;

    if(moveUp)
        m_towerEngine->moveUp();
    else
        m_towerEngine->moveDown();

}

void CVideoController::setLights(bool onOff)
{

    m_stopDetection = true;
    if(m_moveDurationTimer.isActive()){
        m_moveDurationTimer.stop();
        m_towerEngine->stop();
    }

    if(m_detectionThread->isRunning()){
        m_detectionThread->wait();
        m_detectionThread->quit();
    }

    m_towerEngine->turnLights(onOff);
}

bool CVideoController::captureStereoImage(QString leftImageName, QString rightImageName)
{
    static int imgCaptureIndex = 0;

    m_stopDetection = true;
    if(m_detectionThread->isRunning()){
        m_detectionThread->wait();
        m_detectionThread->quit();
    }

    if(m_moveDurationTimer.isActive()){
        m_moveDurationTimer.stop();
        m_towerEngine->stop();
    }

    if(m_videoCapture->isRunning() == false)
        return false;

    std::string leftImageNameC = std::string(), rightImageNameC = std::string();
    if(leftImageName.isEmpty() || rightImageName.isEmpty()){

        setImageNames("captureLeft", "captureRight", imgCaptureIndex, leftImageNameC, rightImageNameC);
        //        leftImageNameC = "captureLeft";
        //        rightImageNameC = "captureRight";

        //        std::ostringstream numberStr;
        //        numberStr<<index;

        //        leftImageNameC = leftImageNameC.append(numberStr.str());
        //        rightImageNameC = rightImageNameC.append(numberStr.str());

        //        leftImageNameC = leftImageNameC.append(".jpg");
        //        rightImageNameC = rightImageNameC.append(".jpg");
        imgCaptureIndex++;
    }else{
        leftImageNameC = leftImageName.toUtf8().data();
        rightImageNameC = rightImageName.toUtf8().data();
    }

    return m_videoCapture->captureStereoImage(leftImageNameC, rightImageNameC);
}

void CVideoController::stopTowerMove()
{
    m_stopDetection = true;
    if(m_moveDurationTimer.isActive()){
        m_moveDurationTimer.stop();
        m_towerEngine->stop();
    }

    m_towerEngine->stop();

    if(m_detectionThread->isRunning()){
        m_detectionThread->wait();
        m_detectionThread->quit();
    }
}

void CVideoController::setDebugMode(bool debug)
{
    m_isDebugMode = debug;
    if (!debug)
        destroyAllWindows();
}

void CVideoController::setEyesImageProvider(EyesImageProvider *imageProvider)
{
    m_eyesImageProvider = imageProvider;
}

void CVideoController::processNewStereoCameraFrame(Mat leftImage, Mat rightImage)
{
//    qDebug()<<"new stereo camera frame";

    if (m_isDebugMode) {
        imshow(CAPTURE_WINDOW1, leftImage);
        imshow(CAPTURE_WINDOW2, rightImage);
    }

    if(m_stopDetection) return;

    if(m_performDetection){
        m_performDetection = false;
        m_detectionThread->detect(leftImage, rightImage, m_detectionType);
    }

}

bool CVideoController::mustStopTower(QPointF &leftEyeLeftImg, QPointF &rightEyeLftImg, QPointF &leftEyeRightImg, QPointF &rightEyeRightImg){

    double eyesAverageHeight = 0;
    if(m_closestCameraToLight == 0)
        eyesAverageHeight = (leftEyeLeftImg.y() + rightEyeLftImg.y())/2;
    else
        eyesAverageHeight = (leftEyeRightImg.y() + rightEyeRightImg.y())/2;

    double threshold = m_imageSize.height()/10;
    if(std::abs(eyesAverageHeight - m_imageSize.height()/2) > threshold)
        return false;

    return true;
}

void CVideoController::onDetectionResult(bool detected, QRectF faceLeftImg, QPointF leftEyeLeftImg, QPointF rightEyeLeftImg, QRectF leftEyeRectLeftImg, QRectF rightEyeRectLeftImg, QRectF faceRightImg, QPointF leftEyeRightImg, QPointF rightEyeRightImg, QRectF leftEyeRectRightImg, QRectF rightEyeRectRightImg)
{

//    qDebug()<<"Detection type is: "<<m_detectionType;

    static int index = 0;

    switch (m_detectionType) {
    case FACE_DETECTION:

        if(detected){

            double threshold = 0; //m_imageSize.height()/10;

            Point2d faceCenterLeftImag, faceCenterRightImg;
            faceCenterLeftImag = Point2d(faceLeftImg.x() + faceLeftImg.width()/2, faceLeftImg.y() + faceLeftImg.height()/2);
            faceCenterRightImg = Point2d(faceRightImg.x() + faceRightImg.width()/2, faceRightImg.y() + faceRightImg.height()/2);

            double centerY = faceCenterLeftImag.y;
            if(m_closestCameraToLight == 1)
                centerY = faceCenterRightImg.y;

            if(centerY > m_imageSize.height() + threshold){
                m_performDetection = true;
                return;
            }

            m_towerEngine->stop();
            m_moveDurationTimer.stop();

            m_detectionType = EYES_DETECTION;
//            qDebug()<<"Face detected at "<<faceLeftImg<<" "<<faceRightImg;
        }
        m_performDetection = true;

        break;
    case EYES_DETECTION:

        if(detected){

            double difference = 0;
            if(m_closestCameraToLight == 0){
                difference = (leftEyeLeftImg.y() + rightEyeLeftImg.y())/2;
            }else{
                difference =(leftEyeRightImg.y() + rightEyeRightImg.y())/2;
            }

            if( difference > m_imageSize.height()/2 ){
//                std::cerr<<"=============== move tower down =================="<<std::endl;
                m_performDetection = true;
                m_towerEngine->moveDown();
                QTest::qSleep(500);
                m_towerEngine->stop();
                m_performDetection = true;
                return;
            }

//            std::cerr<<"Turn lights on";
            /// turn tower light on
            if(!m_towerEngine->light()){
                m_towerEngine->turnLights(true);
            }

            QTest::qSleep(1500);

            Mat leftImage = Mat(), rightImage = Mat();

            std::string leftImageName = std::string(), rightImageName = std::string();
            setImageNames("left", "right", index, leftImageName, rightImageName);

            //            std::string leftImageName = "left";
            //            std::string rightImageName = "right";

            //            std::ostringstream number;
            //            number<<index;
            //            index++;
            //            leftImageName = leftImageName.append(number.str());
            //            rightImageName = rightImageName.append(number.str());

            //            leftImageName = leftImageName.append(".jpg");
            //            rightImageName = rightImageName.append(".jpg");


            m_videoCapture->captureStereoImage(leftImage, rightImage, true, leftImageName, rightImageName);

//            qDebug()<<"Captured image";

            moveCamerasToTop();

            /// turn tower light off
            if(m_towerEngine->light()){
                m_towerEngine->turnLights(false);
            }

            if(m_detectionThread->isRunning()){
//                std::cerr<<"Stop detection thread";
                m_detectionThread->wait();
                m_detectionThread->quit();

            }else{
//                std::cerr<<"----- DETECTION THREAD IS NOT RUNNING --------";
            }

            m_detectionType = PUPILS_DETECTION;

            leftImage.release(); leftImage = Mat();
            rightImage.release(); rightImage = Mat();
            leftImage = imread(leftImageName);
            rightImage = imread(rightImageName);

            index++;

            m_detectionThread->detect(leftImage, rightImage, m_detectionType, false);

        }else{
//            qDebug()<<"Eyes not detected";
            m_eyesDetectionFrames++;
            if (m_eyesDetectionFrames > 20) {
                qDebug() << "======================Eyes not detected in 20 frames!=====================";
                m_performDetection = false;
                emit detectionFailed();
            } else
                m_performDetection = true;
        }

        break;
    case PUPILS_DETECTION:

        if(m_detectionThread->isRunning()){
            m_detectionThread->wait();
            m_detectionThread->quit();
        }

        if(!detected){
            emit detectionFailed();
            return;
        }

        emit eyesDetected();

        std::string leftImageName = std::string(), rightImageName = std::string();
        int nr = index - 1;
        setImageNames("left", "right", nr, leftImageName, rightImageName);

        Mat imageL = imread(leftImageName, 1);
        Mat imageR = imread(rightImageName, 1);
        int linesize = 3;
        Point vPoint(0, linesize), hPoint(linesize, 0);
        Point llPoint = CConversionUtils::qPointToPoint(leftEyeLeftImg);
        line(imageL, llPoint - hPoint, llPoint + hPoint, Scalar(0, 255, 0));
        line(imageL, llPoint - vPoint, llPoint + vPoint, Scalar(0, 255, 0));

        Point lrPoint = CConversionUtils::qPointToPoint(rightEyeLeftImg);
        line(imageL, lrPoint - hPoint, lrPoint + hPoint, Scalar(0, 255, 0));
        line(imageL, lrPoint - vPoint, lrPoint + vPoint, Scalar(0, 255, 0));

        Point rlPoint = CConversionUtils::qPointToPoint(leftEyeRightImg);
        line(imageR, rlPoint - hPoint, rlPoint + hPoint, Scalar(0, 255, 0));
        line(imageR, rlPoint - vPoint, rlPoint + vPoint, Scalar(0, 255, 0));

        Point rrPoint = CConversionUtils::qPointToPoint(rightEyeRightImg);
        line(imageR, rrPoint - hPoint, rrPoint + hPoint, Scalar(0, 255, 0));
        line(imageR, rrPoint - vPoint, rrPoint + vPoint, Scalar(0, 255, 0));

        if (m_isDebugMode) {
            namedWindow(EYES_WINDOW1, WINDOW_NORMAL);
            moveWindow(EYES_WINDOW1, 400, 150);
            imshow(EYES_WINDOW1, imageL);

            namedWindow(EYES_WINDOW2, WINDOW_NORMAL);
            moveWindow(EYES_WINDOW2, 800, 150);
            imshow(EYES_WINDOW2, imageR);
        }

        leftImageName = std::string(); rightImageName = std::string();
        setImageNames("leftImageRes", "rightImageRes", nr, leftImageName, rightImageName);

        if (m_saveImages) {
            imwrite("leftImageRes.jpg", imageL);
            imwrite("rightImageRes.jpg", imageR);
        }

//        std::cerr<<"======================="<<cv::Point(leftEyeLeftImg.x(), leftEyeLeftImg.y())<<" "<<cv::Point2d(leftEyeRightImg.x(), leftEyeRightImg.y())<<std::endl;

        cv::Point3d leftEye3dPos, rightEye3dPos;
        leftEye3dPos = m_stereoSystem->compute3DPositionUndistoredPoints(CConversionUtils::qPointToPoint(leftEyeLeftImg), CConversionUtils::qPointToPoint(leftEyeRightImg));
//                    (cv::Point2d(leftEyeLeftImg.x(), leftEyeLeftImg.y()), cv::Point2d(leftEyeRightImg.x(), leftEyeRightImg.y()));
        rightEye3dPos = m_stereoSystem->compute3DPositionUndistoredPoints(CConversionUtils::qPointToPoint(rightEyeLeftImg), CConversionUtils::qPointToPoint(rightEyeRightImg));
                //(cv::Point2d(rightEyeLeftImg.x(), rightEyeLeftImg.y()), cv::Point2d(rightEyeRightImg.x(), rightEyeRightImg.y()));

        //        QVector3D leftEye3dPos = m_stereoSystem->compute3DPosition(leftEyeLeftImg, leftEyeRightImg);
        //        QVector3D rightEye3dPos = m_stereoSystem->compute3DPosition(rightEyeLeftImg, rightEyeRightImg);

        moveCamerasToTop();

        Mat leftEyeLeftImgeMat = Mat(), rightEyeLeftImgeMat = Mat();
        Mat leftEyeRightImageMat = Mat(), rightEyeRightImageMat = Mat();

        CConversionUtils::setRoi(imageL, CConversionUtils::qRectToRect(leftEyeRectLeftImg), leftEyeLeftImgeMat);
        CConversionUtils::setRoi(imageL, CConversionUtils::qRectToRect(rightEyeRectLeftImg), rightEyeLeftImgeMat);
        CConversionUtils::setRoi(imageR, CConversionUtils::qRectToRect(leftEyeRectRightImg), leftEyeRightImageMat);
        CConversionUtils::setRoi(imageR, CConversionUtils::qRectToRect(rightEyeRectRightImg), rightEyeRightImageMat);

        //        leftEyeLeftImgeMat = Mat(imageL, CConversionUtils::qRectToRect(leftEyeRectLeftImg)/*cv::Rect(leftEyeRectLeftImg.x(), leftEyeRectLeftImg.y(), leftEyeRectLeftImg.width(), leftEyeRectLeftImg.height())*/);
        //        rightEyeLeftImgeMat = Mat(imageL, CConversionUtils::qRectToRect(rightEyeRectLeftImg)/*cv::Rect(rightEyeRectLeftImg.x(), rightEyeRectLeftImg.y(), rightEyeRectLeftImg.width(), rightEyeRectLeftImg.height())*/);

        //        leftEyeRightImageMat = Mat(imageR, CConversionUtils::qRectToRect(leftEyeRectRightImg)/*cv::Rect(leftEyeRectRightImg.x(), leftEyeRectRightImg.y(), leftEyeRectRightImg.width(), leftEyeRectRightImg.height())*/);
        //        rightEyeRightImageMat = Mat(imageR, CConversionUtils::qRectToRect(rightEyeRectRightImg)/*cv::Rect(rightEyeRectRightImg.x(), rightEyeRectRightImg.y(), rightEyeRectRightImg.width(), rightEyeRectRightImg.height())*/);

        m_eyesImageProvider->setLeftEyeImage1(CConversionUtils::mat2QImage(leftEyeLeftImgeMat));
        m_eyesImageProvider->setRightEyeImage1(CConversionUtils::mat2QImage(rightEyeLeftImgeMat));
        m_eyesImageProvider->setLeftEyeImage2(CConversionUtils::mat2QImage(leftEyeRightImageMat));
        m_eyesImageProvider->setRightEyeImage2(CConversionUtils::mat2QImage(rightEyeRightImageMat));

        //---------------------------------------
        // to be deteleted
        //        if(!leftEyeLeftImgeMat.empty())
        //            imshow("LE - left", leftEyeLeftImgeMat);
        //        if(!rightEyeLeftImgeMat.empty())
        //            imshow("RE - left", rightEyeLeftImgeMat);
        //        if(!leftEyeRightImageMat.empty())
        //            imshow("LE - right", leftEyeRightImageMat);
        //        if(!rightEyeRightImageMat.empty())
        //            imshow("RE - right", rightEyeRightImageMat);
        //---------------------------------------

        emit measurementFinished(index - 1, CConversionUtils::point3DToQVector(leftEye3dPos),
                                 CConversionUtils::point3DToQVector(rightEye3dPos));

        break;
    }
}

void CVideoController::onTowerStopMoving()
{

//    qDebug()<<"=============== Tower moved (msec) "<<m_moveDuration<<" ";
    m_moveDuration += TOWER_MOVE_DURATION_MSEC;
    if(m_moveDuration < TOTAL_TOWER_MOVE_DURATION_MSEC){
        if(m_movingDirection == MOVING_DOWN) m_towerEngine->moveDown();
        if(m_movingDirection == MOVING_UP) m_towerEngine->moveUp();
        return;
    }

    if(m_movingDirection == MOVING_DOWN){

//        qDebug()<<"Cameras reached the bottom. Move cameras up and stop camera preview and detection";

        emit detectionFailed();

        moveCamerasToTop();

        if(m_detectionThread->isRunning()){
            m_detectionThread->wait();
            m_detectionThread->quit();
        }
    }
    else
        if(m_movingDirection == MOVING_UP) {
//            qDebug()<<"Cameras reached the top side. Stop moving the cameras and the timer.";
            m_towerEngine->stop();
            m_moveDurationTimer.stop();
        }
}

void CVideoController::stopEyesDetection()
{
//    qDebug() << "CVideoController::stopEyesDetection()";
    m_performDetection = false;
    m_detectionThread->quit();
    m_detectionThread->wait();

    emit detectionFailed();
    moveCamerasToTop();
}

void CVideoController::setImageNames(std::string leftImageBase, std::string rightImageBase, int index, std::string &leftImageName, std::string &rightImageName)
{

    leftImageName = std::string();
    rightImageName = std::string();

    if(leftImageBase.empty() || rightImageBase.empty()) return;

    leftImageName = leftImageBase;
    rightImageName = rightImageBase;

    std::ostringstream numberString;
    numberString<<index;

    leftImageName = leftImageName.append(numberString.str());
    rightImageName = rightImageName.append(numberString.str());

    leftImageName = leftImageName.append(".jpg");
    rightImageName = rightImageName.append(".jpg");

}
