#ifndef CVIDEOCAPTURE_H
#define CVIDEOCAPTURE_H

#include <QObject>
#include <QThread>
#include "opencvheaders.h"

class CVideoCapture : public QThread
{
    Q_OBJECT
public:
    explicit CVideoCapture();
    ~CVideoCapture();

    bool openCamera(int cameraIndex, cv::Size cameraSize, bool showPreview);
    bool openCameras(int cam1Index, int cam2Index, cv::Size cameraSize, bool showPreviews = false);

    bool captureStereoImage(std::string leftImageName, std::string rightImageName);
    bool captureImage(std::string imageName);

    bool areCamerasOpen();

    void closeCameras();
    void stop();

    bool captureStereoImage(Mat &leftImage, Mat &rightImage, bool saveImage = false, std::string leftImageName = "leftImage.jpg", std::string rightImageName = "rightImage.jpg");
protected:
    void run();

signals:
    void newStereoCameraFrame(Mat leftFrame, Mat rightFrame);
    void newCameraFrame(Mat frame);

public slots:

private:
    cv::VideoCapture m_videoCap1;
    cv::VideoCapture m_videoCap2;
    cv::Mat m_camera1Image;
    cv::Mat m_camera2Image;
    cv::Size m_cameraSize;
    bool m_captureFrames;
    bool m_isStereoCamera;

};

#endif // CVIDEOCAPTURE_H
