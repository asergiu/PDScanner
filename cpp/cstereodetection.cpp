#include "cstereodetection.h"
#include "cconversionutils.h"
#include "debug.h"

#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;

#include <iostream>
#include <vector>
using namespace std;

#ifdef DEBUG_EXECUTION_TIME
#include <time.h>
#endif

const cv::Size CStereoDetection::FACE_DETECTION_IMAGE_SZ = cv::Size(320, 240);

CStereoDetection::CStereoDetection()
{
    m_faceCascade = CascadeClassifier();
    m_eyesCascade = CascadeClassifier();

    clearDetectionData();
}

void CStereoDetection::clearDetectionData()
{
    for(int i = 0; i < NUM_IMAGES; i++){
        m_image[i] = Mat();
        m_faceRect[i] = Rect();

        for(int j = 0; j < MAX_EYE_POINTS; j++){
            m_eyesRect[j][i] = Rect();
            m_cornealReflection[j][i] = Point2d();
        }
    }
}

bool CStereoDetection::facesDetected()
{
    return (m_faceRect[LEFT_IMAGE] != Rect() && m_faceRect[RIGHT_IMAGE] != Rect());
}

bool CStereoDetection::eyesDetected()
{
    for(int i = 0; i < NUM_IMAGES; i++)
        for(int j = 0; j < MAX_EYE_POINTS; j++)
            if(m_eyesRect[j][i] == Rect())
                return false;

    return true;
}

Point2d CStereoDetection::getCornealReflection(EYE_POINT_TYPE eyeType, CStereoDetection::ImageType imageType){
    return m_cornealReflection[eyeType][imageType];
}

Rect CStereoDetection::getEyesRectangle(EYE_POINT_TYPE eyeType, CStereoDetection::ImageType imageType){
    return m_eyesRect[eyeType][imageType];
}

Point2d CStereoDetection::getEyesCenter(EYE_POINT_TYPE eyeType, CStereoDetection::ImageType imageType)
{
    Rect eyeRectangle = m_eyesRect[eyeType][imageType];
    if(eyeRectangle == Rect())
        return cv::Point2d();

    return cv::Point2d(eyeRectangle.x + eyeRectangle.width/2, eyeRectangle.y + eyeRectangle.height/2);
}

Rect CStereoDetection::getFaceRectangle(CStereoDetection::ImageType imageType){
    return m_faceRect[imageType];
}

bool CStereoDetection::hasCornealReflection(EYE_POINT_TYPE eyeType, CStereoDetection::ImageType imageType){
    return m_cornealReflection[eyeType][imageType] != cv::Point2d();
}

bool CStereoDetection::hasEyeDetected(EYE_POINT_TYPE eyeType, CStereoDetection::ImageType imageType){
    return m_eyesRect[eyeType][imageType] != Rect();
}

bool CStereoDetection::hasFaceDetected(CStereoDetection::ImageType imageType){
    return m_faceRect[imageType] != Rect();
}

bool CStereoDetection::initDetection(std::string cascadeClassifiersPath)
{
    std::string faceCascade, eyesCascade;

    faceCascade = cascadeClassifiersPath;
    faceCascade.append("haarcascade_frontalface_alt.xml");

    eyesCascade = cascadeClassifiersPath;
    eyesCascade.append("haarcascade_eye_tree_eyeglasses.xml");

    m_faceCascade = CascadeClassifier(faceCascade);
    m_eyesCascade = CascadeClassifier(eyesCascade);

    return !(m_faceCascade.empty() & m_eyesCascade.empty());
}

bool CStereoDetection::isDetectionInitialized()
{
    return !(m_faceCascade.empty() & m_eyesCascade.empty());
}

bool CStereoDetection::loadImages(Mat &leftImage, Mat &rightImage)
{

    if(leftImage.empty() || rightImage.empty())
        return false;

    clearDetectionData();
    m_image[LEFT_IMAGE] = leftImage.clone();
    m_image[RIGHT_IMAGE] = rightImage.clone();

    return true;
}

bool CStereoDetection::areImagesLoaded()
{
    for(int i = 0; i < NUM_IMAGES; i++)
        if(m_image[i].empty())
            return false;

    return true;
}

bool CStereoDetection::detectFaces()
{
    if(!areImagesLoaded()){
        std::cerr<<"Empty detection images"<<std::endl;
        return false;
    }

    Rect faceLeftImage = Rect(), faceRightImage = Rect();
    bool leftImgDet = detectFace(m_image[LEFT_IMAGE], faceLeftImage);
    bool rightImgDet = detectFace(m_image[RIGHT_IMAGE], faceRightImage);

    if(!(leftImgDet & rightImgDet))
        return false;


    m_faceRect[LEFT_IMAGE] = faceLeftImage;
    m_faceRect[RIGHT_IMAGE] = faceRightImage;

    return true;
}

bool CStereoDetection::detectEyes()
{

    if(!areImagesLoaded()){
        std::cerr<<"Empty detection images"<<std::endl;
        return false;
    }

    Rect leftEyeLeftImg = Rect(), rightEyeLeftImg = Rect();
    Rect leftEyeRightImg = Rect(), rightEyeRightImg = Rect();

    Mat leftImage = Mat(), rightImage = Mat();

    if(!facesDetected())
        detectFaces();

    if(!facesDetected()){
//        std::cerr<<"Faces not detected! Searching eyes on the whole image"<<std::endl;
        leftImage = m_image[LEFT_IMAGE];
        rightImage = m_image[RIGHT_IMAGE];
        return false;
    }
    else{

        CConversionUtils::setRoi(m_image[LEFT_IMAGE], m_faceRect[LEFT_IMAGE], leftImage);
        CConversionUtils::setRoi(m_image[RIGHT_IMAGE], m_faceRect[RIGHT_IMAGE], rightImage);
        //        leftImage = Mat(m_image[LEFT_IMAGE], m_faceRect[LEFT_IMAGE]);
        //        rightImage = Mat(m_image[RIGHT_IMAGE], m_faceRect[RIGHT_IMAGE]);
    }

    bool leftImgDet = detectEyes(leftImage, leftEyeLeftImg, rightEyeLeftImg);
    bool rightImgDet = detectEyes(rightImage, leftEyeRightImg, rightEyeRightImg);

    if(!(leftImgDet & rightImgDet))
        return false;


    if(facesDetected()){

        leftEyeLeftImg = leftEyeLeftImg + cv::Point(m_faceRect[LEFT_IMAGE].x, m_faceRect[LEFT_IMAGE].y);
        rightEyeLeftImg = rightEyeLeftImg + cv::Point(m_faceRect[LEFT_IMAGE].x, m_faceRect[LEFT_IMAGE].y);

        leftEyeRightImg = leftEyeRightImg + cv::Point(m_faceRect[RIGHT_IMAGE].x, m_faceRect[RIGHT_IMAGE].y);
        rightEyeRightImg = rightEyeRightImg + cv::Point(m_faceRect[RIGHT_IMAGE].x, m_faceRect[RIGHT_IMAGE].y);
    }

    m_eyesRect[EYE_POINT_LEFT][LEFT_IMAGE] = leftEyeLeftImg;
    m_eyesRect[EYE_POINT_RIGHT][LEFT_IMAGE] = rightEyeLeftImg;
    m_eyesRect[EYE_POINT_LEFT][RIGHT_IMAGE] = leftEyeRightImg;
    m_eyesRect[EYE_POINT_RIGHT][RIGHT_IMAGE] = rightEyeRightImg;


    return true;
}

bool CStereoDetection::detectFace(const cv::Mat &inputImage, cv::Rect &faceRectangle)
{

    faceRectangle = Rect();

    if(inputImage.empty()){
//        std::cerr<<"Empty input image for face detection"<<std::endl;
        return false;
    }

    if(m_faceCascade.empty()){
//        std::cerr<<"Empty cascade classified for face detection"<<std::endl;
        return false;
    }

    double imageScaleFactor = std::max((int)inputImage.size().width/FACE_DETECTION_IMAGE_SZ.width, (int)inputImage.size().height/FACE_DETECTION_IMAGE_SZ.height);

    Mat detectionImageSmall = Mat();
    if(imageScaleFactor >= 1){
        resize(inputImage, detectionImageSmall, Size(inputImage.size().width/imageScaleFactor, inputImage.size().height/imageScaleFactor));
    }
    else
        detectionImageSmall = inputImage.clone();

    Mat grayImage;
    cvtColor(detectionImageSmall, grayImage, CV_BGR2GRAY);
    equalizeHist(grayImage, grayImage);


    std::vector<Rect> faces;
    m_faceCascade.detectMultiScale( grayImage, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30));

    if(faces.empty()){
//        std::cerr<<"No face was detected in the input image"<<std::endl;
        return false;
    }

#ifdef DEBUG_SHOW_FACES
    Mat colorImage;
    colorImage = inputImage.clone();
#endif

    Rect biggestFace = Rect(0, 0, 0, 0);

    for( size_t i = 0; i < faces.size(); i++ )
    {
#ifdef DEBUG_SHOW_FACE
        Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
        ellipse( colorImage, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
#endif
        if(biggestFace.area() < faces[i].area()){
            biggestFace = faces[i];
        }
    }

    faceRectangle = biggestFace;

#ifdef DEBUG_SHOW_FACE
    namedWindow("faces", WINDOW_NORMAL);
    imshow("faces", colorImage);
#endif

    /// convert to original image coordinates

    if(imageScaleFactor >= 1)
        faceRectangle = Rect(faceRectangle.x*imageScaleFactor, faceRectangle.y*imageScaleFactor, faceRectangle.width*imageScaleFactor, faceRectangle.height*imageScaleFactor);


    return true;
}

bool CStereoDetection::detectEyes(Mat inputImage, cv::Rect &leftEye, cv::Rect & rightEye)
{

    rightEye = Rect();
    leftEye = Rect();
    if(inputImage.empty()){
//        std::cerr<<"Empty image for eyes detection";
        return false;
    }

    if(m_eyesCascade.empty()){
//        std::cerr<<"Empty eye cascade classifier"<<std::endl;
        return false;
    }

    std::vector<Rect> eyes;
    m_eyesCascade.detectMultiScale(inputImage, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(20, 20) );

    if(eyes.size() <=1 ){
//        std::cerr<<"Eyes not detected in the input image"<<std::endl;
        return false;
    }


#ifdef DEBUG_SHOW_EYES
    Mat allEyesMat = inputImage.clone();

    for(size_t j = 0; j < eyes.size(); j++){
        rectangle(allEyesMat, eyes.at(j), Scalar(0, 255, 255), 1);
    }

    namedWindow("allEyes", WINDOW_NORMAL);
    imshow("allEyes", allEyesMat);
#endif

    Rect aux = Rect();
    Point2d faceCenter = cv::Point2d(inputImage.size().width/2, inputImage.size().height/2);

    for(size_t i = 0; i < eyes.size() - 1; i++){
        Rect r1 = eyes.at(i);
        for(size_t j = i+1; j < eyes.size(); j++){
            Rect r2 = eyes.at(j);
            if(CConversionUtils::interesct(r1, r2)){
                r1 = CConversionUtils::maxRect(r1, r2);
                eyes.erase(eyes.begin() + j);
                j--;
            }
        }
    }

#ifdef DEBUG_SHOW_EYES
    allEyesMat = inputImage.clone();

    for(size_t j = 0; j < eyes.size(); j++){
        rectangle(allEyesMat, eyes.at(j), Scalar(0, 0, 255), 1);
    }

    namedWindow("allEyesAfterMerge", WINDOW_NORMAL);
    imshow("allEyesAfterMerge", allEyesMat);
#endif

    /// sort rectanles according to their x coodrinate
    for( size_t j = 0; j < eyes.size() ; j++ )
    {
        for(size_t i = j+1; i < eyes.size(); i++){
            if(eyes[i].x < eyes[j].x){
                aux = eyes[i];
                eyes[i] = eyes[j];
                eyes[j] = aux;
            }
        }
    }


    /// find eye pair based on eyes area ratio and position
    double threshold = 0; //faceCenter.x/10;
    for(size_t i = 0; i < eyes.size() - 1; i++){

        if(eyes[i].x + eyes[i].width/2 > faceCenter.x + threshold) break;
        if(eyes[i].y > inputImage.size().height*0.4) continue;

        for(size_t j = i+1; j < eyes.size(); j++){
            if(eyes[j].x + eyes[j].width/2 < faceCenter.x - threshold) continue;
            if(eyes[j].y > inputImage.size().height*0.4) continue;

            if(std::max(eyes[i].area(), eyes[j].area())/std::min(eyes[i].area(), eyes[j].area()) > 1.5)
                continue;

            /// \todo verify slope contraint

            rightEye = eyes[i];
            leftEye = eyes[j];

        }
    }

    if(rightEye == Rect() || leftEye == Rect())
        return false;

#ifdef DEBUG_SHOW_EYES
    Mat displayMat = inputImage.clone();
    rectangle(displayMat, rightEye, Scalar(255, 255, 0), 1);
    rectangle(displayMat, leftEye, Scalar(255, 255, 0), 1);
    namedWindow("eyesdetected", WINDOW_NORMAL);
    imshow("eyesdetected", displayMat);
    waitKey(0);
#endif


    return true;

}

vector<KeyPoint> findSmallBlobs(Mat input){

    Mat out;
    vector<KeyPoint> keyPoints;

    Mat src = input.clone();
    Mat element = getStructuringElement(CV_SHAPE_CROSS, Size(3,  3), Point(1, 1));
    int nr_iterations = 1;
    erode(src, src, element, Point(-1, -1), nr_iterations, cv::BORDER_REPLICATE);
    dilate(src, src, element, Point(-1, -1), nr_iterations, cv::BORDER_REPLICATE);



    //    cv::cvtColor(input, src, CV_BGR2GRAY);
    //    cv::equalizeHist(src, src);

    SimpleBlobDetector::Params params;
    params.minThreshold = 100;
    params.maxThreshold = 200;//200;//128;
    params.thresholdStep = 10;

    params.blobColor = 255;
    params.minArea = 1;
    //    params.minConvexity = 0.3;
    params.minInertiaRatio = 0.01;

    params.maxArea = src.size().width/10;
    //    params.maxConvexity = 10;

    params.filterByColor = true;
    params.filterByCircularity = false;
    params.filterByConvexity = false;
    params.filterByInertia = false;

    SimpleBlobDetector blobDetector( params );
    blobDetector.create("SimpleBlob");

    blobDetector.detect( src, keyPoints );
    drawKeypoints( src, keyPoints, out, CV_RGB(0,255,0), DrawMatchesFlags::DEFAULT);

    //    namedWindow( "image", WINDOW_NORMAL );
    //    imshow( "image", out );
    //    waitKey(0);

//    std::cerr<<keyPoints.size()<<std::endl;
    return keyPoints;

}

bool CStereoDetection::detectCornealReflectionBlob(){
    if(!eyesDetected())
        detectEyes();

    if(!eyesDetected()){
//        std::cerr<<"Eyes not detected in the input images! Cannot search for corneal reflection"<<std::endl;
        return false;
    }

    for(int i = 0; i < MAX_EYE_POINTS; i++){
        Mat eyeLeftImg = Mat(), eyeRightImg = Mat();

        eyeLeftImg = Mat(m_image[LEFT_IMAGE], m_eyesRect[i][LEFT_IMAGE]);
        eyeRightImg = Mat(m_image[RIGHT_IMAGE], m_eyesRect[i][RIGHT_IMAGE]);

        vector<KeyPoint> blobsLeft = findSmallBlobs(eyeLeftImg);
        vector<KeyPoint> blobsRight = findSmallBlobs(eyeRightImg);

//        std::cerr<<"blobs size "<<blobsLeft.size();

        vector<double> blobsLeftRes = evaluateCornealReflectionCandidates(eyeLeftImg, blobsLeft);
        vector<double> blobsRightRes = evaluateCornealReflectionCandidates(eyeRightImg, blobsRight);

        double bestLeftIndex = -1;
        double bestLeft = 1000;
        for(size_t k = 0; k < blobsLeft.size(); k++){
            if(blobsLeftRes.at(k) < bestLeft ){
                bestLeft = blobsLeftRes.at(k);
                bestLeftIndex = k;
            }
        }

        double bestRightIndex = -1;
        double bestRight = 1000;
        for(size_t k = 0; k < blobsRight.size(); k++){
            if(blobsRightRes.at(k) < bestRight ){
                bestRight = blobsRightRes.at(k);
                bestRightIndex = k;
            }
        }

        if(bestRightIndex != -1){
            m_cornealReflection[i][RIGHT_IMAGE] = blobsRight.at(bestRightIndex).pt + cv::Point2f(m_eyesRect[i][RIGHT_IMAGE].x, m_eyesRect[i][RIGHT_IMAGE].y);

        }

        if(bestLeftIndex!=-1){
            m_cornealReflection[i][LEFT_IMAGE] = blobsLeft.at(bestLeftIndex).pt + cv::Point2f(m_eyesRect[i][LEFT_IMAGE].x, m_eyesRect[i][LEFT_IMAGE].y);

        }
    }
    return true;
}

bool CStereoDetection::detectCornealReflection()
{

    if(!eyesDetected())
        detectEyes();

    if(!eyesDetected()){
//        std::cerr<<"Eyes not detected in the input images! Cannot search for corneal reflection"<<std::endl;
        return false;
    }

    bool detected = true;

    for(int i = 0; i < MAX_EYE_POINTS; i++){
        Mat eyeLeftImg = Mat(), eyeRightImg = Mat();

        eyeLeftImg = Mat(m_image[LEFT_IMAGE], m_eyesRect[i][LEFT_IMAGE]);
        eyeRightImg = Mat(m_image[RIGHT_IMAGE], m_eyesRect[i][RIGHT_IMAGE]);

        std::vector<cv::Rect> leftImgCandidates = detectCornealReflectionMorphology(eyeLeftImg);
        std::vector<cv::Rect> rightImgCandidates = detectCornealReflectionMorphology(eyeRightImg);

        std::pair<Point2d, Point2d> bestEyeMatch = findAndMatchCorners(eyeLeftImg, eyeRightImg, leftImgCandidates, rightImgCandidates);

        if(bestEyeMatch.first == Point2d() || bestEyeMatch.second == Point2d()){
//            std::cerr<<"corneal reflection in eye "<<i<<" not detected"<<std::endl;
            detected = false;
            continue;
        }

        // convert to full image coordinates
        cv::Point2d centeredReflection = centerCornealReflection(eyeLeftImg, bestEyeMatch.first);
        if(centeredReflection != cv::Point2d())
            bestEyeMatch.first = centeredReflection;

        centeredReflection = centerCornealReflection(eyeRightImg, bestEyeMatch.second);
        if(centeredReflection != cv::Point2d())
            bestEyeMatch.second = centeredReflection;

        m_cornealReflection[i][LEFT_IMAGE] = bestEyeMatch.first + cv::Point2d(m_eyesRect[i][LEFT_IMAGE].x, m_eyesRect[i][LEFT_IMAGE].y);
        m_cornealReflection[i][RIGHT_IMAGE] = bestEyeMatch.second + cv::Point2d(m_eyesRect[i][RIGHT_IMAGE].x, m_eyesRect[i][RIGHT_IMAGE].y);


    }


    return detected;
}

double dist(cv::Point2d p1, cv::Point2d p2){
    double distance = 0;
    distance = sqrt(pow(p1.x - p2.x, 2.0) + pow(p1.y - p2.y, 2.0));
    return distance;
}

std::pair<cv::Point2d, cv::Point2d> CStereoDetection::findAndMatchCorners(Mat img1, Mat img2, std::vector<cv::Rect> &leftImgContours, std::vector<cv::Rect> &rightImgContours)
{
#ifdef DEBUG_EXECUTION_TIME
    clock_t startTime = clock();
#endif

    int maxCorners = 5;
    if( maxCorners < 1 ) { maxCorners = 1; }

    /// Parameters for Shi-Tomasi algorithm
    double qualityLevel = 0.01;
    double minDistance = 10;
    int blockSize = 7;  // sau 7?
    bool useHarrisDetector = false;
    double k = 0.04;

    /// detecting keypoints
    GoodFeaturesToTrackDetector detector(maxCorners, qualityLevel, minDistance, blockSize, useHarrisDetector, k);

    vector<KeyPoint> keypoints1, keypoints2;
    detector.detect(img1, keypoints1);
    detector.detect(img2, keypoints2);

    /// computing descriptors
    SurfDescriptorExtractor extractor;
    Mat descriptors1, descriptors2;
    extractor.compute(img1, keypoints1, descriptors1);
    extractor.compute(img2, keypoints2, descriptors2);

    /// matching descriptors
    BFMatcher matcher(NORM_L2);
    vector<DMatch> matches;
    matcher.match(descriptors1, descriptors2, matches);

#ifdef DEBUG_EXECUTION_TIME
    std::cerr<<"EXECTUTION TIME "<<(double)(clock()-startTime)/CLOCKS_PER_SEC<<std::endl;
#endif


    double minDist = 100, maxDist = 0;

    for( int i = 0; i < descriptors1.rows; i++ )
    { double dist = matches[i].distance;
        if( dist < minDist ) minDist = dist;
        if( dist > maxDist ) maxDist = dist;
    }


//    std::cerr<<"min distance is: "<<minDist<<" and max distance is: "<<maxDist<<std::endl;
    std::vector< DMatch > goodMatches;

    for( int i = 0; i < descriptors1.rows; i++ )
    { //if( matches[i].distance < 3*minDist )
        /// \todo undistort points for this condition
        ///if(abs(keypoints1[matches[i].queryIdx].pt.x - keypoints2[matches[i].queryIdx].pt.x)<7)
        { goodMatches.push_back( matches[i]); }
    }

#ifdef DEBUG_SHOW_MATCHES
    Mat img_matches;
    namedWindow("matches", WINDOW_NORMAL);
    drawMatches( img1, keypoints1, img2, keypoints2,
                 goodMatches, img_matches, Scalar::all(-1), Scalar::all(-1),
                 vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
    imshow("matches", img_matches);
    waitKey(0);
#endif

    //#ifdef DEBUG_SHOW_MATCHES
    //    // drawing the results
    //    namedWindow("matches", WINDOW_NORMAL);

    //    Mat img_matches;
    //    drawMatches(img1, keypoints1, img2, keypoints2, matches, img_matches, Scalar::all(-1), Scalar::all(-1),
    //                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
    //    imshow("matches", img_matches);
    //    waitKey(0);
    //#endif

    // example: http://docs.opencv.org/doc/tutorials/features2d/feature_homography/feature_homography.html

    std::vector<Point2d> img1Points = std::vector<Point2d>(), img2Points = std::vector<Point2d>();
    for(size_t i = 0; i < goodMatches.size(); i++){
        ///  get the keypoints from the good matches
        img1Points.push_back(keypoints1[goodMatches[i].queryIdx].pt);
        img2Points.push_back(keypoints2[goodMatches[i].queryIdx].pt);
    }

    std::vector<double> img1PointsScores = std::vector<double>(img1Points.size(), 0.0), img2PointsScores = std::vector<double>(img1Points.size(), 0.0);

    for(size_t i = 0; i < img1Points.size(); i++){
        Point2d currentPoint = img1Points.at(i);
        double minDist = img1.size().width*img1.size().height;
        for(size_t j = 0; j < leftImgContours.size(); j++){

            cv::Point2d contourCenter = cv::Point2d(leftImgContours.at(j).x + leftImgContours.at(j).width/2, leftImgContours.at(j).y + leftImgContours.at(j).height/2);
            double distance = dist(currentPoint, contourCenter);

            if(minDist > distance){
                minDist = distance;
                img1PointsScores[i] = minDist;
            }
        }
    }

    for(size_t i = 0; i < img2Points.size(); i++){
        Point2d currentPoint = img2Points.at(i);
        double minDist = img2.size().width*img2.size().height;
        for(size_t j = 0; j < rightImgContours.size(); j++){
            cv::Point2d contourCenter = cv::Point2d(rightImgContours.at(j).x + rightImgContours.at(j).width/2, rightImgContours.at(j).y + rightImgContours.at(j).height/2);
            double distance = dist(currentPoint, contourCenter);
            if(distance < minDist){
                minDist = distance;
                img2PointsScores[i] = minDist;
            }
        }

    }

    int minIndex = -1;
    double min = img1.size().width*img1.size().height;

    for(size_t i = 0; i <goodMatches.size(); i++){
        if((img1PointsScores.at(i) + img2PointsScores.at(i))/2 < min){
            min = (img1PointsScores.at(i) + img2PointsScores.at(i))/2;
            minIndex = i;
        }
    }


    if(minIndex == -1){
        return std::pair<Point2d, Point2d>();
    }

    std::pair<Point2d, Point2d> bestMatch;
    bestMatch.first = img1Points.at(minIndex);
    bestMatch.second = img2Points.at(minIndex);

    //    namedWindow("final result1", WINDOW_NORMAL);
    //    namedWindow("final result2", WINDOW_NORMAL);
    //    circle(img1, img1Points.at(minIndex), 3, Scalar(0, 255, 0), 1);
    //    circle(img2, img2Points.at(minIndex), 3, Scalar(0, 255, 0), 1);
    //    imshow("final result1", img1);
    //    imshow("final result2", img2);
    //    waitKey(0);

    return bestMatch;

}

std::vector<double> CStereoDetection::evaluateCornealReflectionCandidates(Mat &inputImage, std::vector<Point2d> candidates){

    std::vector<double> pointsScore = std::vector<double>();
    if(inputImage.empty() || candidates.empty()){
        std::cerr<<"Empty input image or no corneal reflection candidates"<<std::endl;
        return pointsScore;
    }

    Size neighbourSize = Size(inputImage.size().width/5, inputImage.size().height/5);

    Mat grayImage = Mat();
    cvtColor(inputImage, grayImage, CV_BGR2GRAY);

    int nr_iterations = 1;
    Mat element = getStructuringElement(CV_SHAPE_CROSS, Size(3,  3), Point(1, 1));

    erode(grayImage, grayImage, element, Point(-1, -1), nr_iterations, cv::BORDER_REPLICATE);
    dilate(grayImage, grayImage, element, Point(-1, -1), nr_iterations, cv::BORDER_REPLICATE);

    threshold(grayImage, grayImage, 125, 255, CV_THRESH_OTSU);

    for(size_t i = 0; i < candidates.size(); i++){

        double candidateScore = 1;

        cv::Point2d contourCenter = candidates.at(i);
        Rect x = Rect(contourCenter.x - neighbourSize.width/2, contourCenter.y - neighbourSize.height/2, neighbourSize.width, neighbourSize.height);
        Mat neighbourhoodMat = Mat();

        CConversionUtils::setRoi(grayImage, x, neighbourhoodMat);
        normalize(neighbourhoodMat, neighbourhoodMat, 0.0, 1.0, NORM_MINMAX);

        Scalar averageIntensity = cv::mean(neighbourhoodMat);
        candidateScore = averageIntensity.val[0];

        pointsScore.push_back(candidateScore);
    }

    return pointsScore;
}

std::vector<double> CStereoDetection::evaluateCornealReflectionCandidates(Mat &inputImage, std::vector<KeyPoint> candidates){

    std::vector<double> pointsScore = std::vector<double>();
    if(inputImage.empty() || candidates.empty()){
        std::cerr<<"Empty input image or no corneal reflection candidates"<<std::endl;
        return pointsScore;
    }

    Size neighbourSize = Size(inputImage.size().width/5, inputImage.size().height/5);

    Mat grayImage = Mat();
    cvtColor(inputImage, grayImage, CV_BGR2GRAY);

    int nr_iterations = 1;
    Mat element = getStructuringElement(CV_SHAPE_CROSS, Size(3,  3), Point(1, 1));

    erode(grayImage, grayImage, element, Point(-1, -1), nr_iterations, cv::BORDER_REPLICATE);
    dilate(grayImage, grayImage, element, Point(-1, -1), nr_iterations, cv::BORDER_REPLICATE);

    threshold(grayImage, grayImage, 125, 255, CV_THRESH_OTSU);

    for(size_t i = 0; i < candidates.size(); i++){

        double candidateScore = 1;

        cv::Point2d contourCenter = candidates.at(i).pt;
        Rect x = Rect(contourCenter.x - neighbourSize.width/2, contourCenter.y - neighbourSize.height/2, neighbourSize.width, neighbourSize.height);
        Mat neighbourhoodMat = Mat();

        CConversionUtils::setRoi(grayImage, x, neighbourhoodMat);
        normalize(neighbourhoodMat, neighbourhoodMat, 0.0, 1.0, NORM_MINMAX);

        Scalar averageIntensity = cv::mean(neighbourhoodMat);
        candidateScore = averageIntensity.val[0];

        pointsScore.push_back(candidateScore);
    }

    return pointsScore;
}

std::vector<cv::Rect> CStereoDetection::detectCornealReflectionMorphology(Mat inputImage)
{

    std::vector<cv::Rect> candidates = std::vector<cv::Rect>();
    if(inputImage.empty()){
        std::cerr<<"ERROR!!! Input image empty!";
        return candidates;
    }

    Size neighbourSize = Size(inputImage.size().width/5, inputImage.size().height/5);

    Mat grayImage;
    cvtColor(inputImage, grayImage, CV_BGR2GRAY);
    Mat processedMat = Mat();

    int nr_iterations = 1;
    Mat element = getStructuringElement(CV_SHAPE_CROSS, Size(3,  3), Point(1, 1));

    processedMat = grayImage.clone();
    erode(grayImage, processedMat, element, Point(-1, -1), nr_iterations, cv::BORDER_REPLICATE);
    dilate(grayImage, processedMat, element, Point(-1, -1), nr_iterations, cv::BORDER_REPLICATE);

    threshold(processedMat, processedMat, 125, 255, CV_THRESH_OTSU);

    std::vector<vector<Point> > contour = vector<vector<Point> >();

    findContours(processedMat.clone(), contour, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

    Mat thresholdedImage = processedMat.clone();
    Mat contours_image;
    RNG rng(12345);

    cvtColor(thresholdedImage, contours_image, CV_GRAY2BGR);

    for(size_t index = 0; index < contour.size(); index++){

        Rect contourBBox = boundingRect(contour.at(index));
        // filter out too big contours
        if(contourBBox.width > inputImage.size().width/5 || contourBBox.width > inputImage.size().height/5)
            continue;

        Mat contourMat = Mat(processedMat, contourBBox).clone();
        Scalar contourIntensity = cv::mean(contourMat);

        if(contourIntensity.val[0] < 128)
            continue;

        cv::Point2d contourCenter = cv::Point2d(contourBBox.x + contourBBox.width/2, contourBBox.y + contourBBox.height/2);
        Rect neigbourhoodRect = Rect(contourCenter.x - neighbourSize.width/2, contourCenter.y - neighbourSize.height/2, neighbourSize.width, neighbourSize.height);

        Mat neighbourhoodMat = Mat();
        CConversionUtils::setRoi(processedMat, neigbourhoodRect, neighbourhoodMat);
        normalize(neighbourhoodMat, neighbourhoodMat, 0.0, 1.0, NORM_MINMAX);

        Scalar averageIntensity = cv::mean(neighbourhoodMat);
//        std::cerr<<averageIntensity.val[0]<<std::endl;

        if(averageIntensity.val[0] > 0.4)
            continue;

        Scalar color = Scalar(rng.uniform(0,255), rng.uniform(0, 255), rng.uniform(0, 255));
        drawContours(contours_image, contour, index, color, 1, 8);
        rectangle(contours_image, neigbourhoodRect, Scalar(0, 0, 255), 1);

        candidates.push_back(contourBBox);
    }

    //    destroyWindow("contours");
    //    namedWindow("contours", WINDOW_NORMAL);
    //    imshow("contours", contours_image);
    //    waitKey(0);

    return candidates;
}

cv::Point2d CStereoDetection::centerCornealReflection(Mat inputImage1, cv::Point2d originalReflection){

    if(inputImage1.empty() || originalReflection == cv::Point2d())
        return cv::Point2d();


    Mat inputImage = Mat();
    cvtColor(inputImage1, inputImage, CV_BGR2GRAY);

    int neigbourhoodSize = inputImage.size().width/7;
    Rect neigbourhoodRect = Rect(originalReflection.x - neigbourhoodSize/2,
                                 originalReflection.y - neigbourhoodSize/2,
                                 neigbourhoodSize, neigbourhoodSize);


    Mat roi;
    CConversionUtils::setRoi(inputImage, neigbourhoodRect, roi);

    threshold(roi, roi, 125, 255, THRESH_OTSU);

    vector<vector<Point> > contour = vector<vector<Point> >();
    findContours(roi.clone(), contour, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

#ifdef DEBUG_SHOW_CENTERING
    Mat contoursImage = Mat();
    cvtColor(roi, contoursImage, CV_GRAY2RGB);
#endif

    cv::Point2d cornealReflectionOnROI = originalReflection - cv::Point2d(neigbourhoodRect.x, neigbourhoodRect.y);

#ifdef DEBUG_SHOW_CENTERING
    drawContours(contoursImage, contour, -1, Scalar(255, 0, 255));
    circle(contoursImage, cornealReflectionOnROI, 2, Scalar(0, 255, 0));
#endif

    double minDist = neigbourhoodRect.area();
    double distance;
    int minIndex = -1;

    cv::Point2d bestCenter = cv::Point2d();

    for(size_t index = 0; index < contour.size(); index++){

        Rect bbox = boundingRect(contour.at(index));

        Point2d contourCenter = CConversionUtils::zoneGravityCenter(contour.at(index));
        Mat colorMat = Mat(roi, bbox).clone();
        normalize(colorMat, colorMat, 0.0, 1.0, NORM_MINMAX);
        Scalar averageIntensity = mean(colorMat);

        //        std::cerr<<averageIntensity.val[0]<<std::endl;

        if(averageIntensity.val[0] < 0.6) continue;

        distance = dist(cornealReflectionOnROI, contourCenter);

        if(distance < minDist){
            minIndex = index;
            minDist = distance;
            bestCenter = contourCenter;
        }
    }


#ifdef DEBUG_SHOW_CENTERING
    drawContours(contoursImage, contour, minIndex, Scalar(0, 0, 255));
    namedWindow("contours", WINDOW_NORMAL);
    imshow("contours", contoursImage);
#endif

    if(minIndex != -1){
        bestCenter = bestCenter + cv::Point2d(neigbourhoodRect.x, neigbourhoodRect.y);
    }
    else
        return cv::Point2d();

#ifdef DEBUG_SHOW_CENTERING
    int lineSize = 5;
    cvtColor(inputImage, inputImage, CV_GRAY2BGR);

    line(inputImage, originalReflection + Point2d(0, lineSize), originalReflection + Point2d(0, -lineSize), Scalar(0, 0, 255));
    line(inputImage, originalReflection + Point2d(lineSize, 0), originalReflection + Point2d( -lineSize, 0), Scalar(0, 0, 255));

    line(inputImage, bestCenter + Point2d(0, lineSize), bestCenter + Point2d(0, -lineSize), Scalar(0, 255, 0));
    line(inputImage, bestCenter + Point2d(lineSize, 0), bestCenter + Point2d( -lineSize, 0), Scalar(0, 255, 0));

    namedWindow("finalRes", WINDOW_NORMAL);
    imshow("finalRes", inputImage);
    waitKey(0);
#endif

    return bestCenter;
}


